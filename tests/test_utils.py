import unittest

from app import utils


class UtilsTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_unique_name(self):
        def contains_predicate(item_list):
            return lambda x: x not in item_list

        name = "some-article-title"
        used_names = ["some-article-title", "some-article-title-1", "some-article-title-2"]
        is_unique = contains_predicate(used_names)

        unique_name = utils.unique_name(name, is_unique)

        self.assertTrue(is_unique(unique_name))
        self.assertTrue(unique_name not in used_names)

        used_names.append(unique_name)
        is_unique = contains_predicate(used_names)

        second_unique_name = utils.unique_name(name, is_unique)

        self.assertTrue(is_unique(second_unique_name))
        self.assertTrue(second_unique_name not in used_names)
        self.assertNotEqual(second_unique_name, unique_name)

