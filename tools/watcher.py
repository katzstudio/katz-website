import os
import sys
import time
import errno
from subprocess import call
from watchdog.events import FileSystemEventHandler, FileMovedEvent, FileCreatedEvent, FileModifiedEvent
from watchdog.observers import Observer


def replace_path(filename, folder_from, folder_to):
    if filename == '':
        return filename

    split = os.path.split(filename)
    if split[1] == folder_from:
        return os.path.join(split[0], folder_to)
    else:
        return os.path.join(replace_path(split[0], folder_from, folder_to), split[1])


def make_dirs_with_parents(filename):
    try:
        os.makedirs(filename)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(filename):
            pass
        else:
            raise


class Preprocessor:
    def __init__(self, ext):
        self.ext = ext

    def can_process(self, filename):
        return os.path.splitext(filename)[1] == ("." + self.ext)


class ReactPreprocessor(Preprocessor):
    def __init__(self, root_path):
        Preprocessor.__init__(self, "jsx")
        self.root_path = root_path

    def process(self, jsx_file):
        print("[REACT] Processing %s" % jsx_file)

        js_file = ReactPreprocessor.output_path(jsx_file)
        make_dirs_with_parents(os.path.split(js_file)[0])

        ReactPreprocessor.compile_jsx(jsx_file, js_file)

        view_files = ReactPreprocessor.find_views(self.root_path)
        for view_file in view_files:
            js_file = ReactPreprocessor.output_path(view_file)
            ReactPreprocessor.compile_jsx(view_file, js_file)

    @staticmethod
    def output_path(file):
        out_filename = replace_path(file, "jsx", "js")
        out_filename = os.path.splitext(out_filename)[0] + ".js"
        return out_filename

    @staticmethod
    def compile_jsx(jsx_file, js_file):
        print("[REACT] compiling %s -> %s" % (jsx_file, js_file))
        f = open(js_file, "w")
        call(["browserify", "-g", "reactify", jsx_file], cwd=".", shell=True, stdout=f)
        f.close()

    @staticmethod
    def find_views(root_path):
        views = []
        for file_name in os.listdir(root_path):
            current_file = os.path.join(root_path, file_name)

            if os.path.isdir(current_file) and file_name == 'views':
                for view_file in os.listdir(current_file):
                    if os.path.splitext(view_file)[1] == ".jsx":
                        views.append(os.path.join(current_file, view_file))

            if os.path.isdir(current_file):
                views.extend(ReactPreprocessor.find_views(current_file))

        return views


class SassPreprocessor(Preprocessor):
    def __init__(self, root_path):
        Preprocessor.__init__(self, "scss")
        self.root_path = root_path

    @staticmethod
    def process(file):
        print("[SASS] Processing " + file)

        file_to = os.path.splitext(file)[0] + ".css"
        call(["sass", file, file_to], cwd=".", shell=True)


class SingleSassPreprocessor(Preprocessor):
    """
    Only compiles one main sass file but listens to changes on any sass source file.
    It assumes the main sass file includes every other sass file.
    """
    MAIN_SCSS = "../app/static/css/sass/main.scss"
    MAIN_CSS = "../app/static/css/main.css"

    def __init__(self, root_path):
        # We should respond with True for any scss file, but only process main.scss
        Preprocessor.__init__(self, "scss")
        self.root_path = root_path

    @staticmethod
    def process(file):
        print("[SASS] Processing " + file + "(only compiling main.scss, make sure " + file + " is @included)")

        file_from = SingleSassPreprocessor.MAIN_SCSS
        file_to = SingleSassPreprocessor.MAIN_CSS

        call(["sass", "--sourcemap=none", "--style", "compressed", "--", file_from, file_to], cwd=".", shell=True)


class EventHandler(FileSystemEventHandler):
    def __init__(self, root_path):
        self.root_path = root_path
        self.processors = {SingleSassPreprocessor(root_path), ReactPreprocessor(root_path)}

    def on_any_event(self, event):
        if isinstance(event, FileMovedEvent):
            self.handle(event.dest_path)
        elif isinstance(event, FileCreatedEvent) or isinstance(event, FileModifiedEvent):
            self.handle(event.src_path)

    def handle(self, filename):
        # noinspection PyBroadException
        try:
            for processor in self.processors:
                if processor.can_process(filename):
                    processor.process(filename)
                    return True
        except:
            return False

        return False


if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else '../app/static'

    event_handler = EventHandler(path)

    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    print("Watcher is monitoring " + path)

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
