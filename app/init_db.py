from datetime import datetime, timedelta

from uuid import uuid4

from flask_script import Command

from app import db
from app.auth.models import User
from app.blog.models import Article
from app.gallery.models import Image


class InitializeDatabase(Command):
    @staticmethod
    def run(**kwargs):
        InitializeDatabase.create_database()
        InitializeDatabase.init_users()
        #InitializeDatabase.init_articles()
        #InitializeDatabase.init_gallery()

    @staticmethod
    def create_database():
        db.create_all()

    # noinspection PyArgumentList
    @staticmethod
    def init_users():
        try:
            alex = User(username="alex", name="Alex Pană", email="alex@katzstudio.com",
                        avatar_url="img/avatars/avatar_alex_2.png", roles="Developer and 3D Artist", biography="")
            alex.password = "violent red"
            db.session.add(alex)

            andra = User(username="andra", name="Andra Moroşan", email="andra@katzstudio.com",
                         avatar_url="img/avatars/avatar_andra_2.png", roles="Concept and Texture Artist", biography="")
            andra.password = "cerulean blue"
            db.session.add(andra)

            db.session.commit()
        except:
            pass

    @staticmethod
    def init_articles():
        one_day = timedelta(days=1)
        first_article = Article(
            id=uuid4(), title="First Article", author="alex",
            public_url="first-article",
            intro_paragraph="Introduction to the first article",
            content="Content for the first article", date=datetime.utcnow() - one_day,
            modified=datetime.utcnow(), tags=[], state="published", headline=True)
        db.session.add(first_article)

        second_article = Article(
            id=uuid4(), title="Second Article", author="alex",
            public_url="second-article",
            intro_paragraph="Introduction to the second article",
            content="Content for the second article", date=datetime.utcnow(),
            modified=datetime.utcnow(), tags=[], state="published", headline=True)
        db.session.add(second_article)

        third_article = Article(
            id=uuid4(), title="Third Article", author="alex",
            public_url="third-article",
            intro_paragraph="Introduction to the third article",
            content="Content for the third article", date=datetime.utcnow() + one_day,
            modified=datetime.utcnow(), tags=[], state="published", headline=True)
        db.session.add(third_article)

        db.session.commit()

    @staticmethod
    def init_gallery():
        forest = Image(id=uuid4(),
                       title="Forest",
                       uploader=User.query.get("alex").username,
                       is_banner_candidate=True,
                       fill_size_path="forest_3.png",
                       half_size_path="forest_3_half.png",
                       width=1024,
                       height=512,
                       preferred_ratio=2)

        db.session.add(forest)
        db.session.commit()

