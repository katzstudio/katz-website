from flask import Flask
from flask_login import LoginManager
from flask_misaka import Misaka
from flask_sqlalchemy import SQLAlchemy

from app.config import config
from app.context import context

db = SQLAlchemy()

login_manager = LoginManager()
login_manager.session_protection = 'basic'
login_manager.login_view = 'auth.login'

misaka = Misaka(wrap=True)


def create_app(config_name):
    # create the app
    app = Flask(__name__)

    # apply the configuration
    app_config = config[config_name]
    app.config.from_object(app_config)
    app_config.init_app(app)

    app.logger.info("Created application with configuration: " + config_name)

    app.logger.info("Using database url: " + app.config["SQLALCHEMY_DATABASE_URI"])

    # initialize extensions
    db.init_app(app)
    login_manager.init_app(app)
    misaka.init_app(app)

    # bind the blueprints
    from app.auth import auth as auth_blueprint
    from app.main import main as main_blueprint
    from app.blog import blog as blog_blueprint
    from app.admin import admin as admin_blueprint
    from app.api import api as api_blueprint
    from app.gallery import gallery as gallery_blueprint

    from app.blog import views, api
    from app.gallery import views, api
    from app.auth import views, api
    from app.admin import views

    app.register_blueprint(main_blueprint)
    app.register_blueprint(blog_blueprint, url_prefix="/blog")
    app.register_blueprint(auth_blueprint, url_prefix="/auth")
    app.register_blueprint(gallery_blueprint, url_prefix="/gallery")
    app.register_blueprint(api_blueprint, url_prefix="/api")
    app.register_blueprint(admin_blueprint, url_prefix="/admin")

    @app.context_processor
    def app_context():
        from app.gallery.context import GalleryContext
        from app.auth.context import AuthContext

        __context = context()
        __context['gallery'] = GalleryContext
        __context['auth'] = AuthContext
        return __context

    return app
