__author__ = 'Alex'


def unique_name(name, is_unique, separator="-"):
    """ Generates a name that contains the 'name' argument, but is unique
    according to the 'is_unique' predicate.
    :param name: the original name, which may not be unique
    :param is_unique: a predicate returning whether or not a name is unique
    :param separator: separator used when generating a unique name
    :return: a name which is unique according to the is_unique predicate
    """
    if is_unique(name):
        return name

    postfix = 1
    new_name = name + separator + str(postfix)
    while not is_unique(new_name):
        postfix += 1
        new_name = name + separator + str(postfix)

    return new_name


def to_url(value):
    """ Generates a string that can be used as part of an URL
    :param value: the value to be converted
    :return: a string which can be used as part of an URL
    """
    value = str(value)
    return "-".join(value.split()).lower()
