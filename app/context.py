WEBSITE_NAME = "Katz Studio"

WEBSITE_DESCRIPTION = "Home of the indie game developers Katz!"

WEBSITE_KEYWORDS = "indie, gamedev, games, game development, game art"


def context():
    def title(page_name):
        return WEBSITE_NAME + " - " + page_name

    def description():
        return WEBSITE_DESCRIPTION

    def keywords():
        return WEBSITE_KEYWORDS

    def limit_characters(text, limit):
        if len(text) > limit:
            return text[:limit] + "..."
        return text

    def short_date(date):
        try:
            return date.strftime("%d %b")
        except:
            return "unknown"

    def long_date(date):
        try:
            return date.strftime("%d %b %y")
        except:
            return "unknown"

    def debug(item):
        print(item)
        return ""

    def label_text_for(field):
        return field

    # noinspection PyUnusedLocal
    def ignore(ignored):
        return ""

    def length(collection):
        return len(collection)

    def sort_by_category(flash_messages):
        flash_messages.sort(key=lambda x: x[0])

    def switch(value):
        return "on" if value else "off"

    def date_from_timestamp(timestamp):
        try:
            return timestamp.date()
        except:
            "unknown"

    return dict(
            limit_characters=limit_characters,
            short_date=short_date,
            long_date=long_date,
            debug=debug,
            label_text_for=label_text_for,
            ignore=ignore,
            len=length,
            sort_by_category=sort_by_category,
            switch=switch,
            title=title,
            description=description,
            keywords=keywords,
            date_from_timestamp=date_from_timestamp)
