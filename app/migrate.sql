
-- 2015/12/18 Rename flag columns for Article. Add created and modified timestamps
ALTER TABLE articles DROP date;
ALTER TABLE articles DROP modified;

ALTER TABLE articles ADD created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE articles ADD modified_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

ALTER TABLE articles CHANGE published is_published TINYINT(1) NOT NULL;
ALTER TABLE articles CHANGE deleted is_deleted TINYINT(1) NOT NULL;
ALTER TABLE articles CHANGE headline is_headline TINYINT(1) NOT NULL;

-- 2015/12/20 Add description property to images
ALTER TABLE images ADD description TEXT;

-- 2015/12/28 Change image table structure to allow multiple variants of an image (half, full, thumbnail, etc.)
ALTER TABLE images ADD filename VARCHAR(128) NOT NULL;
ALTER TABLE images ADD extension VARCHAR(8) NOT NULL;
ALTER TABLE images DROP filename_half;

UPDATE images SET extension = substring_index(filename_full, '.', -1);
UPDATE images SET filename = substring_index(filename_full, '.', 1);

ALTER TABLE images DROP filename_full;

ALTER TABLE images ADD total_size_on_disk INTEGER NOT NULL DEFAULT 0;
UPDATE images SET size_on_disk_full = 0 WHERE size_on_disk_full IS NULL;
UPDATE images SET size_on_disk_half = 0 WHERE size_on_disk_half IS NULL;
UPDATE images SET total_size_on_disk = size_on_disk_full + size_on_disk_half;
ALTER TABLE images DROP size_on_disk_full;
ALTER TABLE images DROP size_on_disk_half;

ALTER TABLE images CHANGE COLUMN filename filename VARCHAR(128) NOT NULL AFTER title;
ALTER TABLE images CHANGE COLUMN extension extension VARCHAR(8) NOT NULL AFTER filename;
ALTER TABLE images CHANGE COLUMN width width INT(11) NOT NULL AFTER extension;
ALTER TABLE images CHANGE COLUMN height height INT(11) NOT NULL AFTER width;
ALTER TABLE images CHANGE COLUMN total_size_on_disk total_size_on_disk INT(11) NOT NULL AFTER height;

-- 2015/12/28 Remove alembic_version table
DROP TABLE alembic_version;

-- 2016/01/03 Add support for image visibility
ALTER TABLE images ADD is_visible_in_gallery BOOLEAN DEFAULT FALSE NOT NULL;