from flask import Blueprint
from app import login_manager
from app.auth.models import User

auth = Blueprint('auth', __name__)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


from . import views, models