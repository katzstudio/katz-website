from flask import request
from flask_login import login_user, login_required, current_user

from app.api.response import APIResponse
from app.api import api
from app.auth import User


@api.route("/auth/login", methods=['POST'])
def auth_login():
    username = request.get_json()['username']
    password = request.get_json()['password']

    user = User.query.filter_by(username=username).first()
    if user is not None and user.verify_password(password):
        login_user(user, True)
        return APIResponse.success().json()

    return APIResponse.failure().reason("Invalid username or password").json()


@api.route("/auth/logged_user", methods=['POST'])
@login_required
def logged_user():
    response_data = {
        "user": {
            "name": current_user.name
        }
    }
    return APIResponse.success().data(response_data).json()
