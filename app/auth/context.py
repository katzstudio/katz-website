from flask_login import current_user


class AuthContext:
    @staticmethod
    def user():
        return current_user
