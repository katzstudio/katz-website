from flask import render_template, redirect, url_for
from flask_login import logout_user

from app.auth import auth


@auth.route("/login", methods=['GET', 'POST'])
def login():
    page = {
        'js': 'auth/views/login.js'
    }
    return render_template("auth/login.html", page=page)


@auth.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("auth.login"))
