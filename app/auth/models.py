from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db


class User(UserMixin, db.Model):
    __tablename__ = "users"

    username = db.Column(db.String(32), unique=True, primary_key=True)
    name = db.Column(db.String(32))
    email = db.Column(db.String(32), unique=True)
    password_hash = db.Column(db.String(128))
    avatar_url = db.Column(db.String(128))
    roles = db.Column(db.String(128))
    biography = db.Column(db.Text)

    @property
    def password(self):
        raise AttributeError("password is a write only attribute")

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_id(self):
        return self.username
