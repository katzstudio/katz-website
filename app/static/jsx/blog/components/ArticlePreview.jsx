require("../../libs/API.js");
require("../../gifplayer.jsx");

ArticlePreview = React.createClass({
    getInitialState: function () {
        return {content: this.props.content};
    },

    componentWillReceiveProps: function (nextProps) {
        API.blog.markdown(nextProps.content, $.proxy(function (response) {
            $(this.refs.content).empty();
            $(this.refs.content).append($(response['markdown']))
            GIFPlayer();
        }, this));
    },

    render: function () {
        return (
            <div className="preview">
                <div className="article">
                    <div className="article-header">
                        <span className="title">{this.props.title}</span>
                    </div>
                    <div className="article-content">
                        <span className="content" ref="content"></span>
                    </div>
                </div>
            </div>);
    }
});