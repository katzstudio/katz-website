ArticleWriter = React.createClass({
    getInitialState: function () {
        return {
            articleId: this.props.articleId,
            title: '',
            content: ''
        }
    },

    getArticle: function () {
        return {
            id: this.state.articleId,
            title: this.state.title,
            content: this.state.content
        };
    },

    notifyArticleChange: function () {
        if (this.props.onArticleChange !== undefined) {
            this.props.onArticleChange(this.getArticle());
        }
    },

    onTitleChange: function (newTitle) {
        console.log("New Title: " + newTitle);
        this.setState({title: newTitle});
        this.notifyArticleChange();
    },

    onContentChange: function (newContent) {
        console.log("New Content: " + newContent);
        this.setState({content: newContent});
        this.notifyArticleChange();
    },

    componentWillMount: function () {
        API.blog.get_article(this.state.articleId, $.proxy(function (response) {
            this.setState({title: response['title'], content: response['content']});
            this.notifyArticleChange();
        }, this));
    },

    render: function () {
        return (
            <div className="blog-admin edit-article">
                <ArticleEditor title={this.state.title} content={this.state.content} onTitleChange={this.onTitleChange} onContentChange={this.onContentChange}/>
                <ArticlePreview title={this.state.title} content={this.state.content}/>
            </div>
        );
    }
});