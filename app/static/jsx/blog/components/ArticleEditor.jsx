function substr(e, from, to) {
    return e.val().substring(from, to);
}

ArticleEditor = React.createClass({
    componentDidMount: function () {
        var title = $(this.refs.title);
        var content = $(this.refs.content);

        title.on("input", $.proxy(function () {
            this.props.onTitleChange(title.val());
        }, this));

        content.on("input", $.proxy(function () {
            this.props.onContentChange(content.val());
        }, this));


        content.on('keydown', $.proxy(function (e) {
            if ((e.which == '66') && (e.ctrlKey || e.metaKey)) {
                e.stopPropagation();
                this.onBold();
            }

            if ((e.which == '73') && (e.ctrlKey || e.metaKey)) {
                e.stopPropagation();
                this.onItalic();
            }
        }, this));
    },

    componentWillReceiveProps: function (newProps) {
        var title = $(this.refs.title);
        var content = $(this.refs.content);

        title.val(newProps.title);
        content.val(newProps.content);
    },

    insert: function (text, position) {
        var content = $(this.refs.content);

        if (position == undefined) {
            position = content.getCursor();
        }
        content.val(substr(content, 0, position) + text + substr(content, position));

        this.props.onContentChange(content.val());
    },

    decorate: function (decoration) {
        var content = $(this.refs.content);

        var selection = content.getSelection();
        if (selection[0] == selection[1]) {
            return;
        }

        content.val(substr(content, 0, selection[0]) +
            decoration +
            substr(content, selection[0], selection[1]) +
            decoration +
            substr(content, selection[1]));

        this.props.onContentChange(content.val());
    },

    onBold: function () {
        this.decorate("**");
    },

    onItalic: function () {
        this.decorate("*");
    },
    onInsertLink: function () {
        this.insert("[example link](http://example.com/)");
    },

    onInsertImage: function () {
        this.insert("![alt text](/path/to/img.jpg \"Title\")");
    },

    render: function () {
        return (
            <div className="editor">
                <span className="header">Title</span>
                <input className="title" ref="title"/>

                <span className="header">Content</span>

                <div className="content-actions">
                    <div className="button" onClick={this.onBold}><i className="fa fa-bold"/></div>
                    <div className="button" onClick={this.onItalic}><i className="fa fa-italic"/></div>
                    <div className="button" onClick={this.onInsertLink}><i className="fa fa-link"/></div>
                    <div className="button" onClick={this.onInsertImage}><i className="fa fa-image"/></div>
                </div>
                <textarea className="article" ref="content"/>
            </div>
        );
    }
});