require("../../libs/API.js");


ArticleList = React.createClass({
    getInitialState: function () {
        return {articles: []};
    },

    fetchData: function () {
        API.blog.all($.proxy(function (response) {
            this.setState({articles: response});
        }, this));
    },

    componentDidMount: function () {
        this.fetchData();
        setInterval(this.fetchData, this.props.pollInterval);
    },

    componentWillUnmount: function () {
        clearInterval(this.fetchData);
    },

    redirect: function (url) {
        return function () {
            window.location = url;
        }
    },

    render: function () {
        var articles = this.state.articles.filter(function(article) {
            return !article.deleted;
        });

        var redirect = this.redirect;
        var articleRows = articles.map(function (article) {
            var title_url = "/admin/articles/write/" + article.id;
            var article_title = article.title !== '' ? article.title : <i>Untitled</i>;
            var state = article.published ? "Published" : "Draft";
            return (
                <div className="row" id={article.id} onClick={redirect(title_url)}>
                    <div className="cell title">{article_title}</div>
                    <div className="cell state">{state}</div>
                    <div className="cell author">{article.author}</div>
                    <div className="cell date-created">{article.created.date}</div>
                    <div className="cell date-modified">{article.modified.date}</div>
                </div>
            )
        });
        return (
            <div className="blog-admin article-list">
                <div className="table modern">
                    <div className="header">
                        <div className="cell title">Title</div>
                        <div className="cell state">State</div>
                        <div className="cell author">Author</div>
                        <div className="cell date-created">Date Created</div>
                        <div className="cell date-modified">Last Modified</div>
                    </div>
                    {articleRows}
                </div>
            </div>
        );
    }
});
