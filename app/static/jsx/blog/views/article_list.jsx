require("../components/ArticleList.jsx");
require("../../admin/components/Menu.jsx");
require("../../admin/components/ActionBar.jsx");

ReactDOM.render(<ArticleList pollInterval={2000}/>, $(".content")[0]);

ReactDOM.render(<Menu active="Blog"/>, $(".content-wrap .menu")[0]);

var actions = [
    {
        name: "Create Article",
        icon: "fa-plus",
        href: "/admin/articles/write/"
    }
];

ReactDOM.render(<ActionBar actions={actions}/>, $('.buttons-wrap')[0]);