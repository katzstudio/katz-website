require("../components/ArticleEditor.jsx");
require("../components/ArticlePreview.jsx");
require("../components/ArticleWriter.jsx");

require("../../libs/Popover.js");

require("../../components/Dialog.jsx");

require("../../admin/components/ActionBar.jsx");
require("../../admin/components/Menu.jsx");

var article = {
    id: getArticleIdFromUrl(),
    title: '',
    content: '',
    published: false
};

function getArticleIdFromUrl() {
    var url = window.location.pathname;
    if (url.endsWith("/")) {
        url = url.substr(0, url.length - 1);
    }
    var parts = url.split("/");
    return parts[parts.length - 1];
}

function onArticleChange(newArticle) {
    article.title = newArticle.title;

    article.content = newArticle.content;
}

ReactDOM.render(<ArticleWriter onArticleChange={onArticleChange} articleId={getArticleIdFromUrl()}/>, $('.content')[0]);

ReactDOM.render(<Menu active="Blog"/>, $(".content-wrap .menu")[0]);

function onDeleteButtonClick(deleteButton) {
    var deleteDialog = React.createElement(Dialog, {
        title: "Confirmation",
        message: "Are you sure you want to permanently destroy any trace of this article?",
        onDelete: function () {
            deleteButton.setIcon("fa-cog fa-spin");
            Popover.hide();
            API.blog.delete_article(article.id, function (response, status) {
                if (status['success'] == true) {
                    deleteButton.setIcon("fa-check");
                    setTimeout(function () {
                        window.location = "/admin/articles";
                    }, 800);
                } else {
                    console.log("Could not delete article.");
                    console.log(status);
                    deleteButton.setIcon("fa-question");
                    setTimeout(function () {
                        deleteButton.setIcon("fa-trash");
                    }, 2000);
                }
            });
        },
        onCancel: function () {
            Popover.hide();
        }
    });

    Popover.showReact(deleteDialog);
}

function onSaveButtonClick(saveButton) {
    saveButton.setIcon("fa-cog fa-spin");
    API.blog.save_article(article, function () {
        saveButton.setIcon("fa-check");
        setTimeout(function () {
            saveButton.setIcon("fa-save");
        }, 2000);
    });
}

function onSaveButtonInit(saveButton) {
    $(document).keydown(function (e) {
        if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
            e.stopPropagation();
            onSaveButtonClick(saveButton);
            return false;
        }
        return true;
    });
}

function onPublishButtonClick(publishButton) {
    function updateButtonFromStatus(success, published) {
        if (success === true) {
            publishButton.setIcon("fa-check");
            publishButton.setText(published ? "Published" : "Unpublished");
            article.published = published;
        } else {
            publishButton.setIcon("fa-question");
            publishButton.setText("Failed");
        }
        setTimeout(function () {
            updatePublishButton(publishButton, article.published);
        }, 2000);
    }

    publishButton.setIcon("fa-cog fa-spin");

    var apiFunction = article.published ? API.blog.unpublish_article : API.blog.publish_article;
    var postActionPublishStatus = article.published ? false : true;

    apiFunction(article.id, function (response, status) {
        updateButtonFromStatus(status.success, postActionPublishStatus);

        if (status.success !== true) {
            console.log(article.published ? "Unpublish" : "Publish" + " failed for reason: " + status.reason);
        }
    });
}

function updatePublishButton(publishButton, published) {
    if (published) {
        publishButton.setText("Unpublish");
        publishButton.setIcon("fa-cloud-download");
        publishButton.setClass("blue margin-left");
    } else {
        publishButton.setText("Publish");
        publishButton.setIcon("fa-cloud-upload");
        publishButton.setClass("green margin-left");
    }
}
function onPublishButtonInit(publishButton) {
    API.blog.get_article(article.id, function (response) {
        article.published = response['published'];
        updatePublishButton(publishButton, response['published']);
    });
}

var actions = [
    {
        name: "Save",
        icon: "fa-save",
        onClick: onSaveButtonClick,
        onInit: onSaveButtonInit
    },
    {
        name: "Delete",
        icon: "fa-trash",
        class: "red",
        onClick: onDeleteButtonClick
    },
    {
        name: "Publish",
        icon: "fa-cloud-download",
        text: "Unpublish",
        class: "green margin-left",
        onClick: onPublishButtonClick,
        onInit: onPublishButtonInit
    }
];

ReactDOM.render(<ActionBar actions={actions}/>, $('.buttons-wrap')[0]);