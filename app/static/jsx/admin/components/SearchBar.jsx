SearchBar = React.createClass({
    onSearch: function (value) {
        if (this.props.onSearch !== undefined) {
            this.props.onSearch(value);
        }
    },

    onKeyDown: function (event) {
        if (event.which == 13) {
            var input = $(this.refs.input);
            var search = $(this.refs.search);
            if (input.val() != "") {
                this.props.onSearch(input.val());
                input.val("");
            }
            search.removeClass("extended");
            $(input).blur();
        }
    },

    onClick: function () {
        var search = $(this.refs.search);
        var input = $(this.refs.input);
        if (!search.hasClass("extended")) {
            search.addClass("extended");
            input.focus();
        }
    },

    onBlur: function () {
        $(this.refs.search).removeClass("extended");
    },

    render: function () {
        return (
            <div className="search" ref="search" onClick={this.onClick}>
                <i className="fa fa-search"/>
                <input ref="input" spellCheck="false" onKeyDown={this.onKeyDown} onBlur={this.onBlur}/>
            </div>
        );
    }
});