Menu = React.createClass({
    getDefaultProps: function () {
        return {
            items: [
                {name: "Dashboard", icon: "fa-dashboard", url: "#", disabled: true},
                {name: "Blog", icon: "fa-pencil", url: "/admin/articles"},
                {name: "Gallery", icon: "fa-camera", url: "/admin/gallery"},
                {name: "Users", icon: "fa-user", url: "#", disabled: true},
                {name: "Settings", icon: "fa-cog", url: "#", disabled: true},
                {name: "Messages", icon: "fa-envelope", url: "#", disabled: true}
            ]
        };
    },

    render: function () {
        var activeItem = this.props.active;
        var items = this.props.items.map(function (item) {
            var iconClass = "fa " + item.icon;
            var wrapperClass = "item";
            if (item.name === activeItem) {
                wrapperClass += " active";
            }
            if (item.disabled) {
                wrapperClass += " disabled";
            }

            return (<a href={item.url} key={item.name}><div className={wrapperClass}>
                <i className={iconClass}/>
            </div></a>);
        });
        return (<div>{items}</div>);
    }
});