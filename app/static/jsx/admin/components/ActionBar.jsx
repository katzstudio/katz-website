Action = React.createClass({
    getInitialState: function () {
        return {
            'icon': this.props.action.icon,
            'text': this.props.action.text,
            'class': this.props.action.class !== undefined ? this.props.action.class : '',
            'href': this.props.action.href
        }
    },

    setText: function (text) {
        this.setState({text: text});
    },

    setIcon: function (icon) {
        this.setState({icon: icon});
    },

    setClass: function (buttonClass) {
        this.setState({class: buttonClass});
    },

    onClick: function (event) {
        this.props.action.onClick(this);
    },

    componentDidMount: function () {
        if (this.props.action.onInit !== undefined) {
            this.props.action.onInit(this);
        }
    },

    render: function () {
        var iconClass = "fa " + this.state.icon;
        var textComponent = undefined;
        if (this.props.action.text !== undefined && this.state.text != "") {
            textComponent = <span ref="text">{this.state.text}</span>;
        }

        var buttonClass = "button " + this.state.class;

        var button = (
            <div className={buttonClass} onClick={this.onClick}>
                <i className={iconClass} ref="icon"/>
                {textComponent}
            </div>);

        if (this.props.action.href !== undefined) {
            return <a href={this.props.action.href}>{button}</a>
        } else {
            return button;
        }
    }
});

ActionBar = React.createClass({
    render: function () {
        var buttons = this.props.actions.map(function (action, index) {
            return (<Action action={action} key={index}/>);
        });

        return (
            <div className="buttons">
                {buttons}
            </div>
        );
    }
});