UploadLoadingBar = React.createClass({
    getInitialState: function () {
        return {
            filename: 'Forest_01.jpeg',
            percent: 0,
            file_size: "5Mb"
        }
    },

    onUploadAnother: function () {
        if (this.props.onUploadAnother !== undefined) {
            this.props.onUploadAnother();
        }
    },

    onClose: function () {
        Popover.hide();
    },

    render: function () {
        var percent = this.props.status.percent;
        var totalSize = this.props.status.file_size;
        var uploaded = (totalSize * percent) / 100;

        var filenameInfo;
        if (this.props.status.percent < 100) {
            filenameInfo = "Uploading " + this.props.status.filename;
        } else {
            filenameInfo = "Uploaded " + this.props.status.filename;
        }

        var progressInfo;
        if (this.props.status.percent < 100) {
            progressInfo = humanizeMemorySize(uploaded) + " / " + humanizeMemorySize(totalSize);
        } else {
            progressInfo = humanizeMemorySize(totalSize);
        }

        var loadingFillStyle = {
            width: this.props.status.percent + "%"
        };

        var buttonsStyle = {
            transition: "0.3s",
            height: percent == 100 ? "56px" : "0px",
            overflow: "hidden",
            paddingTop: "10px"
        };

        return (
            <div className="upload-component">
                <h1>Upload</h1>
                <div className="progress-bar">
                    <div className="info-wrap">
                        <div className="percent">{ percent }%</div>
                        <div className="info">
                            <span className="first-line">{filenameInfo}</span>
                            <span className="second-line">{progressInfo}</span>
                        </div>

                    </div>
                    <div className="fill" style={loadingFillStyle}>
                    </div>
                </div>
                <div className="buttons" style={buttonsStyle}>
                    <div className="button blue" onClick={this.onUploadAnother}>Another One</div>
                    <div className="button red" onClick={this.onClose}>Close</div>
                </div>
            </div>
        );
    }
});

UploadDropzone = React.createClass({
    waitingMessage: "Drop image here to upload",

    hoveredMessage: "Drop it like it's hot!",

    getInitialState: function () {
        return {hovered: false};
    },

    componentDidMount: function () {
        var dropzoneAndChildren = $(".dropzone *");

        dropzoneAndChildren.bind('dragenter', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: true});
        }, this));

        dropzoneAndChildren.bind('dragover', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();
        }, this));

        dropzoneAndChildren.bind('dragleave', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: false});
        }, this));

        dropzoneAndChildren.bind('drop', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: false});
            this.props.onDrop(event);
        }, this))
    },

    render: function () {
        var dropzoneClass = 'dropzone ' + (this.state.hovered ? 'active' : '');
        var message = this.state.hovered ? this.hoveredMessage : this.waitingMessage;
        return (
            <div className="upload-component waiting">
                <div className={dropzoneClass}>
                    <span className="message">{message}</span>
                </div>
            </div>
        );
    }
});

Upload = React.createClass({
    getInitialState: function () {
        return {
            uploading: false
        };
    },

    onUploadSuccess: function (response) {
        console.log("onUploadSuccess " + response);
    },

    onUploadProgress: function (progress) {
        console.log("onUploadProgress " + progress);
        this.setState({progress: progress});
    },

    onDrop: function (event) {
        var file = event.originalEvent.dataTransfer.files[0];
        API.gallery.upload_image(file, this.onUploadSuccess, this.onUploadProgress);
        this.setState({uploading: true, progress: 0, file: file});
    },

    onUploadAnother: function () {
        this.setState({uploading: false});
    },

    render: function () {
        if (this.state.uploading) {
            var uploadStatus = {
                filename: this.state.file.name,
                percent: this.state.progress,
                file_size: this.state.file.size
            };
            return (<UploadLoadingBar status={uploadStatus} onUploadAnother={this.onUploadAnother}/>);
        } else {
            return (<UploadDropzone onDrop={this.onDrop}/>);
        }
    }
})
;
