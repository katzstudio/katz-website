Dialog = React.createClass({
    render: function () {
        return (
            <div className="dialog-component">
                <h1>{this.props.title}</h1>
                <span className="message">{this.props.message}</span>
                <div className="buttons">
                    <div className="button red" onClick={this.props.onDelete}>Delete</div>
                    <div className="button blue" onClick={this.props.onCancel}>Cancel</div>
                </div>
            </div>
        );
    }
});