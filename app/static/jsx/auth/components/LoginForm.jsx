require("../../libs/API.js");

LoginForm = React.createClass({
    getInitialState: function () {
        return {'loading': false};
    },

    componentDidMount: function () {
        var username = $(this.refs.username);
        var password = $(this.refs.password);

        var returnKeyHandler = $.proxy(function (event) {
            if (event.which == 13) {
                this.onLogin();
                event.stopPropagation();
            }
        }, this);

        username.on("keypress", returnKeyHandler);

        password.on("keypress", returnKeyHandler);
    },

    onLogin: function () {
        this.setState({'loading': true});
        var username = $(this.refs.username).val();
        var password = $(this.refs.password).val();
        API.auth.login(username, password, $.proxy(function (responseData, responseStatus) {
            if (responseStatus['success']) {
                window.location = "/admin/";
            } else {
                this.setState({'loading': false});
                var error = $(this.refs.error);
                error.html(responseStatus['reason']);
                error.slideDown()
            }
        }, this));
    },

    render: function () {
        var buttonContent = '';
        if (this.state.loading) {
            buttonContent = <i className="fa fa-refresh fa-spin"/>
        } else {
            buttonContent = "Log In";
        }
        return (
            <div className="overlay">
                <div className="login-form-wrap">
                    <div className="login-form">
                        <span className="title">Log In</span>
                        <div className="error" ref="error"></div>
                        <i className="fa fa-user"/>
                        <input ref="username"/>
                        <i className="fa fa-lock"/>
                        <input ref="password" type="password"/>
                        <div className="button" onClick={this.onLogin}>{buttonContent}</div>
                    </div>
                </div>
            </div>
        );
    }
});