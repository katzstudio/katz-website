require("../components/Image.jsx");
require("../components/ImageList.jsx");
require("../components/ImagePreview.jsx");

require("../../admin/components/ActionBar.jsx");
require("../../admin/components/SearchBar.jsx");
require("../../admin/components/Menu.jsx");

require("../../components/Upload.jsx");

ReactDOM.render(<ImageList pollInterval={2000}/>, $(".content")[0]);

var galleryActions = [
    {
        name: "Upload Image",
        icon: "fa-plus",
        onClick: function () {
            Popover.showReact(React.createElement(Upload));
        }
    }
];

ReactDOM.render(<ActionBar actions={galleryActions}/>, $('.buttons-wrap')[0]);

ReactDOM.render(<Menu active="Gallery"/>, $(".content-wrap .menu")[0]);

ReactDOM.render(<SearchBar onSearch={ImageListSearch.search}/>, $('.search-wrap')[0]);