require("../../libs/API.js");
require("../../libs/Matcher.js");


ImageListSearch = {};

ImageList = React.createClass({
    getInitialState: function () {
        return {data: [], matcher: EmptyMatcher};
    },

    fetchData: function () {
        var that = this;
        API.gallery.all(function (response) {
            that.setState({images: response});
        });
    },

    componentDidMount: function () {
        ImageListSearch.search = $.proxy(function (query) {
            this.state.matcher = new Matcher(query);
        }, this);

        ImageListSearch.clearSearch = $.proxy(function (query) {
            this.state.matcher = EmptyMatcher;
        }, this);

        this.fetchData();
        setInterval(this.fetchData, this.props.pollInterval);
    },

    componentWillUnmount: function () {
        clearInterval(this.fetchData);

        ImageListSearch.search = undefined;
        ImageListSearch.clearSearch = undefined;
    },

    hasImages: function () {
        return this.state.images !== undefined;
    },

    render: function () {
        var searchFilter = '';
        var hasFilter = false;
        if (this.state.matcher !== EmptyMatcher) {
            searchFilter =
                <div className="filter">Only showing images with name: <span
                    className="value">{this.state.matcher.query}</span>
                    <i className="fa fa-close" onClick={ImageListSearch.clearSearch}></i>
                </div>;
        }

        var imageNodes = "";
        if (this.hasImages()) {
            imageNodes = this.state.images.map($.proxy(function (image) {
                if (this.state.matcher.matches(image.title)) {
                    return (<Image image={image} key={image.src}/>);
                } else {
                    return '';
                }

            }, this));
        }

        return (
            <div className="full-width gallery-admin group">
                {searchFilter} <br/>
                {imageNodes}
            </div>
        );
    }
});
