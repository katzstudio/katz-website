require("../../libs/Popover.js");

ImagePreview = React.createClass({
    onClick: function () {
        Popover.hide();
    },

    render: function () {
        return (
            <div className="image-preview">
                <img src={this.props.imageSource} className="image" onClick={this.onClick}/>
            </div>
        );
    }
});
