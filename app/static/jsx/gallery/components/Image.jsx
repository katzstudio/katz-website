require("../../components/Dialog.jsx");

require("../../libs/Popover.js");
require("../../libs/Forms.js");
require("../../libs/API.js");
require("../../libs/std.js");

Image = React.createClass({

    componentDidMount: function () {
        var imageId = this.props.image.id;

        this.deleteDialog = React.createElement(Dialog, {
            title: "Confirmation",
            message: "Are you sure you want to permanently remove this image?",
            onDelete: function () {
                API.gallery.delete_image(imageId, function (response, status) {
                    if (status.success) {
                        Popover.hide();
                    } else {
                        console.log(status);
                    }
                });
            },
            onCancel: function () {
                Popover.hide();
            }
        });

        API.gallery.image_descriptor($.proxy(function (data) {
            this.form = new Form(data);
            this.form.setTitle("Edit Image");
            this.form.onCancel(function () {
                Popover.hide();
            });
            this.form.onApply($.proxy(function () {
                API.gallery.update_image(this.form.getValue(), $.proxy(function (response, status) {
                    if (status["success"]) {
                        Popover.hide();
                    } else {
                        this.form.setErrors(response.errors);
                    }
                }, this));
            }, this));
        }, this));
    },

    onEdit: function (event) {
        API.gallery.get_image(this.props.image.id, $.proxy(function (data) {
            this.form.setValue(data);
            this.form.clearErrors();
            Popover.showElement(this.form.element);
        }, this));
        event.stopPropagation();
    },

    onDelete: function (event) {
        Popover.showReact(this.deleteDialog);
        event.stopPropagation();
    },

    onCopy: function () {
        event.stopPropagation();
    },

    onPreview: function () {
        Popover.showReact(React.createElement(ImagePreview, {imageSource: this.props.image.src}), "scale");
    },

    render: function () {
        var headerFlag = '';
        if (this.props.image.banner_candidate) {
            headerFlag = <div className="header-flag" title="This image is a header candidate">header</div>
        }
        return (
            <div className="item">
                <div className="thumbnail" style={{'backgroundImage': 'url(' + this.props.image.src + ')'}}
                     onClick={this.onPreview}>
                    <div className="edit-wrap"><i className="fa fa-trash-o red" onClick={this.onDelete}/></div>
                    <div className="edit-wrap"><i className="fa fa-pencil blue" onClick={this.onEdit}/></div>
                    <a href={this.props.image.src}>
                        <div className="edit-wrap"><i className="fa fa-link blue" onClick={this.onCopy}/></div>
                    </a>
                    <div className="info group">
                        <span className="title">{this.props.image.title}</span><br/>
                        <span className="details">{this.props.image.w} x {this.props.image.h}
                            , {humanizeMemorySize(this.props.image.size)}</span>
                        {headerFlag}
                    </div>
                </div>
            </div>);
    }
});