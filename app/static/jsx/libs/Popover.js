Popover = new (function () {
    var CONTAINER_ID = "ktzPopoverContainer";

    var ELEMENT_REACT = 0;
    var ELEMENT_DOM = 1;
    var ELEMENT_NONE = 2;

    var install = $.proxy(function () {
        if (isInstalled()) {
            return;
        }

        var body = $("body");
        body.append(this.cover);
        body.keydown(function (event) {
            if (event.which == 27) {
                Popover.hide();
            }
        });

        console.log("Popover installed");
    }, this);

    var isInstalled = function () {
        return $("#" + CONTAINER_ID).length == 1;
    };

    var removeAnimationClasses = $.proxy(function () {
        var container = this.container;
        container.classList().forEach(function (element) {
            if (element.startsWith("animation-")) {
                container.removeClass(element);
            }
        });
    }, this);

    var animateShow = $.proxy(function (animationClass) {
        animationClass = animationClass || "default";
        removeAnimationClasses();
        this.container.addClass("animation-show-" + animationClass);

        this.cover.css("visibility", "visible");
        this.cover.css("opacity", "0");
        this.cover.animate({
            opacity: 1.0
        }, 100);
    }, this);

    var animateHide = $.proxy(function (animation) {
        removeAnimationClasses();
        animation = animation || "default";
        this.container.addClass("animation-hide-" + animation);

        // animate opacity to 0 then set visibility to hidden
        this.cover.animate({
                opacity: .0
            }, 100, $.proxy(function () {
                this.cover.css("visibility", "hidden");
            }, this)
        );
    }, this);

    this.showElement = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountDomElement(element);
        animateShow(animation);
    };

    this.showReact = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountReact(element);
        animateShow(animation);
    };

    this.hide = function () {
        animateHide(this.animation);
    };

    var mountReact = $.proxy(function (element) {
        ReactDOM.render(element, this.container[0]);
        this.elementType = ELEMENT_REACT;
    }, this);

    var unmountReact = $.proxy(function () {
        ReactDOM.unmountComponentAtNode(this.container[0]);
        this.elementType = ELEMENT_NONE;
    }, this);

    var mountDomElement = $.proxy(function (element) {
        this.clientElement = element;
        this.container.append(this.clientElement);
        this.elementType = ELEMENT_DOM;
    }, this);

    var unmountDomElement = $.proxy(function () {
        if (this.clientElement !== undefined) {
            this.clientElement.detach();
        }
        this.elementType = ELEMENT_NONE;
    }, this);

    var unmountClient = $.proxy(function () {
        if (this.elementType == ELEMENT_DOM) {
            unmountDomElement();
        }
        if (this.elementType == ELEMENT_REACT) {
            unmountReact();
        }
    }, this);

    // Constructor
    /**
     * Container for the actual popover contents
     */
    this.container = $("<div></div>");
    this.container.addClass("content");

    /**
     * The cover that hides the underlying page
     */
    this.cover = $("<div>");
    this.cover.addClass("modal-popup");
    this.cover.attr("id", CONTAINER_ID);
    this.cover.css("opacity", "0");

    this.cover.append(this.container);

    install();
})();
