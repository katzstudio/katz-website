/**
 * Sets the selection of the editor. If the end of the selection is not
 * specified, then the cursor is placed at the start of the selection.
 *
 * @method setSelection
 * @param {Number} from the start of the selection
 * @param {Number} to the end of the selection.
 */
new function ($) {
    $.fn.setSelection = function (from, to) {
        if (typeof from === 'undefined') {
            return;
        }

        to = (typeof to !== 'undefined' ? to : from);

        var element = this[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(from, to);
        } else if (element.createTextRange) {
            var range = element.createTextRange();
            range.collapse(true);
            range.moveStart('character', from);
            range.moveEnd('character', to);
            range.select();
        }
    }
}(jQuery);

/**
 * Sets the cursor position inside the editor.
 *
 * @method setCursor
 * @param {Number} cursorPosition the new position of the cursor.
 */
new function ($) {
    $.fn.setCursor = function (cursorPosition) {
        return $.fn.setSelection(cursorPosition);
    }
}(jQuery);

/**
 * Returns the selection of the editor.
 *
 * @method getSelection
 * @return {Array} an array containing the [start, end] of the selection
 */
new function ($) {
    $.fn.getSelection = function () {
        var element = this[0];
        return [element.selectionStart, element.selectionEnd]
    }
}(jQuery);

/**
 * Returns the current position of the cursor inside the editor
 *
 * @method getCursor
 * @return {Number} the position of the cursor
 */
new function ($) {
    $.fn.getCursor = function () {
        var element = this[0];
        return element.selectionEnd;
    }
}(jQuery);

/**
 * Returns a list of all the classes of an element
 *
 * @method classList
 * @return {Array} a list of classes associated with the element
 */
new function ($) {
    $.fn.classList = function () {
        return this[0].className.split(/\s+/);
    };
}(jQuery);