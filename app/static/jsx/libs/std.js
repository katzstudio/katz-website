/**
 * Created by Alex on 30/11/2015.
 */

humanizeMemorySize = function humanizeMemorySize(bytes) {
    bytes = parseInt(bytes);
    var unitMultiples = ["b", "Kb", "Mb", "Gb", "Tb"];
    var unit = 0;
    while (bytes > 1000) {
        unit += 1;
        bytes /= 1000;
    }
    return parseInt(bytes * 100) / 100 + unitMultiples[unit];
};
