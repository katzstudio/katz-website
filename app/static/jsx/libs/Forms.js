uuid4 = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        //noinspection JSUnresolvedFunction,JSUnresolvedVariable
        var r = crypto.getRandomValues(new Uint8Array(1))[0] % 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

isEnterKey = function (key) {
    return key == 13;
};

Form = function (modelDescriptor) {

    this.init = function () {
        this.id = uuid4();

        this.fields = [];

        this.descriptor = modelDescriptor;

        this.applyButton = $('<a>').addClass("button").addClass("blue").html("Save");
        this.cancelButton = $("<a>").addClass("button").addClass("red").html("Cancel");

        modelDescriptor['fields'].forEach($.proxy(function (fieldDescriptor) {
            var field = this.createField(fieldDescriptor);
            this.appendField(field);
        }, this));

        this.render();
    };

    this.setTitle = function (title) {
        this.header.html(title);
    };

    this.createField = function (fieldDescriptor) {
        switch (fieldDescriptor.type) {
            case "Text":
                var textField = new TextField(fieldDescriptor);
                textField.editor.keypress($.proxy(function (event) {
                    if (isEnterKey(event.which)) {
                        this.applyButton.click();
                        event.preventDefault();
                    }
                }, this));
                return textField;
            case "Boolean":
                return new BooleanField(fieldDescriptor);
            case "Choice":
                return new ChoiceField(fieldDescriptor);
            case "Hidden":
                return new HiddenField(fieldDescriptor);
        }
    };

    this.appendField = function (field) {
        this.fields.push(field);
    };

    this.render = function () {
        this.element = $("<div>")
            .attr("id", this.id)
            .addClass("form")
            .addClass("dialog");

        this.header = $("<h1>")
            .html(this.descriptor['title']);

        this.errors = $("<div>").addClass("errors").hide();

        var buttonWrap = $("<div>")
            .addClass("buttons")
            .append(this.applyButton)
            .append(this.cancelButton);

        this.element.append(this.header);

        this.element.append(this.errors);

        this.fields.forEach($.proxy(function (field) {
            this.element.append(field.element);
        }, this));

        this.element.append(buttonWrap);

        return this.element;
    };

    this.setValue = function (object) {
        this.fields.forEach(function (field) {
            field.setValue(object[field.descriptor['name']]);
        });
    };

    this.getValue = function () {
        var object = {};
        this.fields.forEach(function (field) {
            object[field.descriptor['name']] = field.getValue();
        });
        return object;
    };

    this.onApply = function (callback) {
        this.applyButton.click(callback);
    };

    this.onCancel = function (callback) {
        this.cancelButton.click(callback);
    };

    this.setErrors = function (errors) {
        var errorList = $("<ul>");
        errors.forEach(function (error) {
            var errorElement = $("<li>").html(error);
            errorList.append(errorElement);
        });
        this.errors.append(errorList);
        this.errors.slideDown();
    };

    this.clearErrors = function () {
        this.errors.empty();
        this.errors.hide();
    };

    this.init();
};

/**
 * Field used to edit lines of text
 */
function TextField(fieldDescriptor) {

    this.init = function () {
        this.descriptor = fieldDescriptor;

        this.elementClass = "_field__" + this.descriptor['name'];

        this.element = $("<div>");

        this.editor = $("<input>")
            .addClass("text-field")
            .addClass(this.elementClass);

        var label = $("<div>")
            .addClass("label")
            .html(this.descriptor['title']);

        this.element.append(this.editor);
        this.element.append(label);
    };

    this.getValue = function () {
        return this.editor.val();
    };

    this.setValue = function (value) {
        this.editor.val(value);
    };

    this.init();
}

/**
 * Field used to edit boolean values
 */
function BooleanField(fieldDescriptor) {
    this.init = function () {
        this.descriptor = fieldDescriptor;

        this.elementClass = "_field__" + this.descriptor['name'].replace(" ", "");

        this.element = $("<div>");
        this.element.addClass("boolean-field");
        this.element.addClass(this.elementClass);

        var label = $('<div class="label">' + this.descriptor['title'] + '</div>');

        this.editor = $("<div>");
        this.editor.addClass("editor");
        this.editor.click($.proxy(function () {
            this.toggleValue();
        }, this));

        this.checkMarkWrap = $('<div class="check-mark-wrap"></div>');
        this.checkMark = $('<i class="fa fa-check check-mark off"></i>');

        this.checkMarkWrap.append(this.checkMark);

        var details = $('<span class="details">' + this.descriptor['description'] + '</span>');

        this.editor.append(this.checkMarkWrap);
        this.editor.append(details);

        this.element.append(label);
        this.element.append(this.editor);
    };

    this.getValue = function () {
        return this.checkMark.hasClass('on');
    };

    this.setValue = function (value) {
        if (value != this.getValue()) {
            this.checkMark.removeClass("on").removeClass("off").addClass(value ? "on" : "off");
        }
        this.element.value = value;
    };

    this.toggleValue = function () {
        this.setValue(!this.getValue());
    };

    this.init();
}

/**
 * Field used to edit enums
 */
function ChoiceField(fieldDescriptor) {
    this.init = function () {
        this.descriptor = fieldDescriptor;
        this.elementClass = "_field__" + this.descriptor['name'];


        this.element = $("<div>");
        this.element.addClass("select-field");
        this.element.addClass(this.elementClass);

        var title = $("<div>");
        title.addClass("title");
        title.html(this.descriptor['title']);
        this.element.append(title);

        var icon = $("<i>");
        icon.addClass("fa").addClass("fa-chevron-down");
        this.element.append(icon);

        this.editor = $("<select>");
        this.editor.addClass("editor");
        this.element.append(this.editor);

        this.descriptor['choices'].forEach($.proxy(function (item) {
            var option = $("<option>");
            option.attr("value", item);
            option.html(item);
            this.editor.append(option);
        }, this));
    };

    this.getValue = function () {
        return this.editor.value;
    };

    this.setValue = function (value) {
        if (this.descriptor['choices'].indexOf(value) >= 0) {
            this.editor.value = value;
        }
    };

    this.init();
}

function HiddenField(descriptor) {
    this.descriptor = descriptor;

    this.element = '';

    this.value = '';

    this.setValue = function (value) {
        this.value = value;
    };

    this.getValue = function () {
        return this.value;
    };

    this.render = function () {
        return "";
    }
}