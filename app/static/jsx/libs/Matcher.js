/**
 * Creates a Matcher object used to apply searcth queries over items or arrays of items.
 *
 * @param query a user search query
 */
Matcher = function (query) {

    // the search query is split into lowercase words
    this.words = query.split(' ').map(function (word) {
        return word.toLowerCase();
    });

    this.query = query;

    /**
     * Tests a single value against a matcher
     * @param matcher the matcher used to test the value against
     * @param value the requested value
     * @returns {*} true if the matcher matches the value. false otherwise
     */
    function matchesSingleValue(matcher, value) {
        if (value.constructor != String) {
            return false;
        }

        var partialMatches = matcher.words.map(function (word) {
            return value.toLocaleLowerCase().includes(word);
        });

        return partialMatches.reduce(function (acc, current) {
            return acc && current;
        }, true);
    }

    /**
     * Tests a single value or an array of values against a matcher
     * @param value either one String or an Array of Strings to be matched by the matcher
     * @returns {*} either true or false if a single value was matched by the string, or an array of match results
     */

    this.matches = function (value) {
        var thisMatcher = this;
        if (value.constructor == String) {
            return matchesSingleValue(thisMatcher, value);
        }

        if (value.constructor == Array) {
            return value.map(function (item) {
                return matchesSingleValue(thisMatcher, item);
            });
        }

        return false;
    }
};

/**
 * Empty matcher that matches ANYTHING
 * @type {Matcher}
 */
EmptyMatcher = new Matcher('');