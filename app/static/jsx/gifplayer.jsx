GIFPlayer = function () {
    $("img.gif").each(function (index, element) {
        var previewFrame = element.src;
        var actualGif = element.src.substr(0, element.src.length - 12) + ".gif";
        var e = $(element);

        var tmpImg = $(new Image());
        tmpImg.attr("src", e.attr('src'));
        tmpImg.on("load", function () {
            var overlay = e.next();
            overlay.width(e.width());
            overlay.height(e.height());
            overlay.css("margin-top", -overlay.height());

        });

        e.on("click", function () {
            console.log("click");
            if (e.attr("src") === previewFrame) {
                e.attr("src", actualGif);
                e.addClass("playing");
                e.removeClass("paused");
            } else {
                e.attr("src", previewFrame);
                e.addClass("paused");
                e.removeClass("playing");
            }
        });
    });
};