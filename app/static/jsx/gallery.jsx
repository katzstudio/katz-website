require("./libs/API.js");

window.openGallery = function openGallery(index) {
    var galleryElement = document.querySelectorAll('.pswp')[0];

    API.gallery.all(function (response) {
        var items = response;

        var options = {
            closeEl: true,
            captionEl: true,
            fullscreenEl: false,
            zoomEl: true,
            shareEl: false,
            counterEl: true,
            arrowEl: true,
            preloaderEl: true,
            index: index,
            showAnimationDuration: 333,
            showHideOpacity: true,
            getThumbBoundsFn: false,
            addCaptionHTMLFn: function (item, captionEl, isFake) {
                var caption = $(captionEl.children[0]);
                caption.empty();

                var title = $("<div>");
                title.addClass('title');
                title.html(item.title);

                var description = $("<div>");
                description.addClass('description');
                description.html(item.description);

                caption.append(title).append(description);
            }
        };

        var gallery = new PhotoSwipe(galleryElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    });
};

/**
 * Returns the index of the image opened in the gallery by inspecting the URL.
 */
function getOpenedGalleryItem() {
    var url_hash = window.location.hash;
    var URL_REGEX = new RegExp("#&gid=([0-9]*)&pid=([0-9]*)");

    var match = URL_REGEX.exec(url_hash);

    if (match != null) {
        return match[2];
    } else {
        return null;
    }
}

// Open the gallery at the item indicated by the URL
var openedGalleryItem = getOpenedGalleryItem();
if (openedGalleryItem != null) {
    openGallery(openedGalleryItem);
}