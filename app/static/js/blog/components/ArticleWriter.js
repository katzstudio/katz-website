(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
ArticleWriter = React.createClass({displayName: "ArticleWriter",
    getInitialState: function () {
        return {
            articleId: this.props.articleId,
            title: '',
            content: ''
        }
    },

    getArticle: function () {
        return {
            id: this.state.articleId,
            title: this.state.title,
            content: this.state.content
        };
    },

    notifyArticleChange: function () {
        if (this.props.onArticleChange !== undefined) {
            this.props.onArticleChange(this.getArticle());
        }
    },

    onTitleChange: function (newTitle) {
        console.log("New Title: " + newTitle);
        this.setState({title: newTitle});
        this.notifyArticleChange();
    },

    onContentChange: function (newContent) {
        console.log("New Content: " + newContent);
        this.setState({content: newContent});
        this.notifyArticleChange();
    },

    componentWillMount: function () {
        API.blog.get_article(this.state.articleId, $.proxy(function (response) {
            this.setState({title: response['title'], content: response['content']});
            this.notifyArticleChange();
        }, this));
    },

    render: function () {
        return (
            React.createElement("div", {className: "blog-admin edit-article"}, 
                React.createElement(ArticleEditor, {title: this.state.title, content: this.state.content, onTitleChange: this.onTitleChange, onContentChange: this.onContentChange}), 
                React.createElement(ArticlePreview, {title: this.state.title, content: this.state.content})
            )
        );
    }
});

},{}]},{},[1]);
