(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
function substr(e, from, to) {
    return e.val().substring(from, to);
}

ArticleEditor = React.createClass({displayName: "ArticleEditor",
    componentDidMount: function () {
        var title = $(this.refs.title);
        var content = $(this.refs.content);

        title.on("input", $.proxy(function () {
            this.props.onTitleChange(title.val());
        }, this));

        content.on("input", $.proxy(function () {
            this.props.onContentChange(content.val());
        }, this));


        content.on('keydown', $.proxy(function (e) {
            if ((e.which == '66') && (e.ctrlKey || e.metaKey)) {
                e.stopPropagation();
                this.onBold();
            }

            if ((e.which == '73') && (e.ctrlKey || e.metaKey)) {
                e.stopPropagation();
                this.onItalic();
            }
        }, this));
    },

    componentWillReceiveProps: function (newProps) {
        var title = $(this.refs.title);
        var content = $(this.refs.content);

        title.val(newProps.title);
        content.val(newProps.content);
    },

    insert: function (text, position) {
        var content = $(this.refs.content);

        if (position == undefined) {
            position = content.getCursor();
        }
        content.val(substr(content, 0, position) + text + substr(content, position));

        this.props.onContentChange(content.val());
    },

    decorate: function (decoration) {
        var content = $(this.refs.content);

        var selection = content.getSelection();
        if (selection[0] == selection[1]) {
            return;
        }

        content.val(substr(content, 0, selection[0]) +
            decoration +
            substr(content, selection[0], selection[1]) +
            decoration +
            substr(content, selection[1]));

        this.props.onContentChange(content.val());
    },

    onBold: function () {
        this.decorate("**");
    },

    onItalic: function () {
        this.decorate("*");
    },
    onInsertLink: function () {
        this.insert("[example link](http://example.com/)");
    },

    onInsertImage: function () {
        this.insert("![alt text](/path/to/img.jpg \"Title\")");
    },

    render: function () {
        return (
            React.createElement("div", {className: "editor"}, 
                React.createElement("span", {className: "header"}, "Title"), 
                React.createElement("input", {className: "title", ref: "title"}), 

                React.createElement("span", {className: "header"}, "Content"), 

                React.createElement("div", {className: "content-actions"}, 
                    React.createElement("div", {className: "button", onClick: this.onBold}, React.createElement("i", {className: "fa fa-bold"})), 
                    React.createElement("div", {className: "button", onClick: this.onItalic}, React.createElement("i", {className: "fa fa-italic"})), 
                    React.createElement("div", {className: "button", onClick: this.onInsertLink}, React.createElement("i", {className: "fa fa-link"})), 
                    React.createElement("div", {className: "button", onClick: this.onInsertImage}, React.createElement("i", {className: "fa fa-image"}))
                ), 
                React.createElement("textarea", {className: "article", ref: "content"})
            )
        );
    }
});

},{}]},{},[1]);
