(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
Action = React.createClass({displayName: "Action",
    getInitialState: function () {
        return {
            'icon': this.props.action.icon,
            'text': this.props.action.text,
            'class': this.props.action.class !== undefined ? this.props.action.class : '',
            'href': this.props.action.href
        }
    },

    setText: function (text) {
        this.setState({text: text});
    },

    setIcon: function (icon) {
        this.setState({icon: icon});
    },

    setClass: function (buttonClass) {
        this.setState({class: buttonClass});
    },

    onClick: function (event) {
        this.props.action.onClick(this);
    },

    componentDidMount: function () {
        if (this.props.action.onInit !== undefined) {
            this.props.action.onInit(this);
        }
    },

    render: function () {
        var iconClass = "fa " + this.state.icon;
        var textComponent = undefined;
        if (this.props.action.text !== undefined && this.state.text != "") {
            textComponent = React.createElement("span", {ref: "text"}, this.state.text);
        }

        var buttonClass = "button " + this.state.class;

        var button = (
            React.createElement("div", {className: buttonClass, onClick: this.onClick}, 
                React.createElement("i", {className: iconClass, ref: "icon"}), 
                textComponent
            ));

        if (this.props.action.href !== undefined) {
            return React.createElement("a", {href: this.props.action.href}, button)
        } else {
            return button;
        }
    }
});

ActionBar = React.createClass({displayName: "ActionBar",
    render: function () {
        var buttons = this.props.actions.map(function (action, index) {
            return (React.createElement(Action, {action: action, key: index}));
        });

        return (
            React.createElement("div", {className: "buttons"}, 
                buttons
            )
        );
    }
});

},{}],2:[function(require,module,exports){
Menu = React.createClass({displayName: "Menu",
    getDefaultProps: function () {
        return {
            items: [
                {name: "Dashboard", icon: "fa-dashboard", url: "#", disabled: true},
                {name: "Blog", icon: "fa-pencil", url: "/admin/articles"},
                {name: "Gallery", icon: "fa-camera", url: "/admin/gallery"},
                {name: "Users", icon: "fa-user", url: "#", disabled: true},
                {name: "Settings", icon: "fa-cog", url: "#", disabled: true},
                {name: "Messages", icon: "fa-envelope", url: "#", disabled: true}
            ]
        };
    },

    render: function () {
        var activeItem = this.props.active;
        var items = this.props.items.map(function (item) {
            var iconClass = "fa " + item.icon;
            var wrapperClass = "item";
            if (item.name === activeItem) {
                wrapperClass += " active";
            }
            if (item.disabled) {
                wrapperClass += " disabled";
            }

            return (React.createElement("a", {href: item.url, key: item.name}, React.createElement("div", {className: wrapperClass}, 
                React.createElement("i", {className: iconClass})
            )));
        });
        return (React.createElement("div", null, items));
    }
});

},{}],3:[function(require,module,exports){
function substr(e, from, to) {
    return e.val().substring(from, to);
}

ArticleEditor = React.createClass({displayName: "ArticleEditor",
    componentDidMount: function () {
        var title = $(this.refs.title);
        var content = $(this.refs.content);

        title.on("input", $.proxy(function () {
            this.props.onTitleChange(title.val());
        }, this));

        content.on("input", $.proxy(function () {
            this.props.onContentChange(content.val());
        }, this));


        content.on('keydown', $.proxy(function (e) {
            if ((e.which == '66') && (e.ctrlKey || e.metaKey)) {
                e.stopPropagation();
                this.onBold();
            }

            if ((e.which == '73') && (e.ctrlKey || e.metaKey)) {
                e.stopPropagation();
                this.onItalic();
            }
        }, this));
    },

    componentWillReceiveProps: function (newProps) {
        var title = $(this.refs.title);
        var content = $(this.refs.content);

        title.val(newProps.title);
        content.val(newProps.content);
    },

    insert: function (text, position) {
        var content = $(this.refs.content);

        if (position == undefined) {
            position = content.getCursor();
        }
        content.val(substr(content, 0, position) + text + substr(content, position));

        this.props.onContentChange(content.val());
    },

    decorate: function (decoration) {
        var content = $(this.refs.content);

        var selection = content.getSelection();
        if (selection[0] == selection[1]) {
            return;
        }

        content.val(substr(content, 0, selection[0]) +
            decoration +
            substr(content, selection[0], selection[1]) +
            decoration +
            substr(content, selection[1]));

        this.props.onContentChange(content.val());
    },

    onBold: function () {
        this.decorate("**");
    },

    onItalic: function () {
        this.decorate("*");
    },
    onInsertLink: function () {
        this.insert("[example link](http://example.com/)");
    },

    onInsertImage: function () {
        this.insert("![alt text](/path/to/img.jpg \"Title\")");
    },

    render: function () {
        return (
            React.createElement("div", {className: "editor"}, 
                React.createElement("span", {className: "header"}, "Title"), 
                React.createElement("input", {className: "title", ref: "title"}), 

                React.createElement("span", {className: "header"}, "Content"), 

                React.createElement("div", {className: "content-actions"}, 
                    React.createElement("div", {className: "button", onClick: this.onBold}, React.createElement("i", {className: "fa fa-bold"})), 
                    React.createElement("div", {className: "button", onClick: this.onItalic}, React.createElement("i", {className: "fa fa-italic"})), 
                    React.createElement("div", {className: "button", onClick: this.onInsertLink}, React.createElement("i", {className: "fa fa-link"})), 
                    React.createElement("div", {className: "button", onClick: this.onInsertImage}, React.createElement("i", {className: "fa fa-image"}))
                ), 
                React.createElement("textarea", {className: "article", ref: "content"})
            )
        );
    }
});

},{}],4:[function(require,module,exports){
require("../../libs/API.js");
require("../../gifplayer.jsx");

ArticlePreview = React.createClass({displayName: "ArticlePreview",
    getInitialState: function () {
        return {content: this.props.content};
    },

    componentWillReceiveProps: function (nextProps) {
        API.blog.markdown(nextProps.content, $.proxy(function (response) {
            $(this.refs.content).empty();
            $(this.refs.content).append($(response['markdown']))
            GIFPlayer();
        }, this));
    },

    render: function () {
        return (
            React.createElement("div", {className: "preview"}, 
                React.createElement("div", {className: "article"}, 
                    React.createElement("div", {className: "article-header"}, 
                        React.createElement("span", {className: "title"}, this.props.title)
                    ), 
                    React.createElement("div", {className: "article-content"}, 
                        React.createElement("span", {className: "content", ref: "content"})
                    )
                )
            ));
    }
});

},{"../../gifplayer.jsx":8,"../../libs/API.js":9}],5:[function(require,module,exports){
ArticleWriter = React.createClass({displayName: "ArticleWriter",
    getInitialState: function () {
        return {
            articleId: this.props.articleId,
            title: '',
            content: ''
        }
    },

    getArticle: function () {
        return {
            id: this.state.articleId,
            title: this.state.title,
            content: this.state.content
        };
    },

    notifyArticleChange: function () {
        if (this.props.onArticleChange !== undefined) {
            this.props.onArticleChange(this.getArticle());
        }
    },

    onTitleChange: function (newTitle) {
        console.log("New Title: " + newTitle);
        this.setState({title: newTitle});
        this.notifyArticleChange();
    },

    onContentChange: function (newContent) {
        console.log("New Content: " + newContent);
        this.setState({content: newContent});
        this.notifyArticleChange();
    },

    componentWillMount: function () {
        API.blog.get_article(this.state.articleId, $.proxy(function (response) {
            this.setState({title: response['title'], content: response['content']});
            this.notifyArticleChange();
        }, this));
    },

    render: function () {
        return (
            React.createElement("div", {className: "blog-admin edit-article"}, 
                React.createElement(ArticleEditor, {title: this.state.title, content: this.state.content, onTitleChange: this.onTitleChange, onContentChange: this.onContentChange}), 
                React.createElement(ArticlePreview, {title: this.state.title, content: this.state.content})
            )
        );
    }
});

},{}],6:[function(require,module,exports){
require("../components/ArticleEditor.jsx");
require("../components/ArticlePreview.jsx");
require("../components/ArticleWriter.jsx");

require("../../libs/Popover.js");

require("../../components/Dialog.jsx");

require("../../admin/components/ActionBar.jsx");
require("../../admin/components/Menu.jsx");

var article = {
    id: getArticleIdFromUrl(),
    title: '',
    content: '',
    published: false
};

function getArticleIdFromUrl() {
    var url = window.location.pathname;
    if (url.endsWith("/")) {
        url = url.substr(0, url.length - 1);
    }
    var parts = url.split("/");
    return parts[parts.length - 1];
}

function onArticleChange(newArticle) {
    article.title = newArticle.title;

    article.content = newArticle.content;
}

ReactDOM.render(React.createElement(ArticleWriter, {onArticleChange: onArticleChange, articleId: getArticleIdFromUrl()}), $('.content')[0]);

ReactDOM.render(React.createElement(Menu, {active: "Blog"}), $(".content-wrap .menu")[0]);

function onDeleteButtonClick(deleteButton) {
    var deleteDialog = React.createElement(Dialog, {
        title: "Confirmation",
        message: "Are you sure you want to permanently destroy any trace of this article?",
        onDelete: function () {
            deleteButton.setIcon("fa-cog fa-spin");
            Popover.hide();
            API.blog.delete_article(article.id, function (response, status) {
                if (status['success'] == true) {
                    deleteButton.setIcon("fa-check");
                    setTimeout(function () {
                        window.location = "/admin/articles";
                    }, 800);
                } else {
                    console.log("Could not delete article.");
                    console.log(status);
                    deleteButton.setIcon("fa-question");
                    setTimeout(function () {
                        deleteButton.setIcon("fa-trash");
                    }, 2000);
                }
            });
        },
        onCancel: function () {
            Popover.hide();
        }
    });

    Popover.showReact(deleteDialog);
}

function onSaveButtonClick(saveButton) {
    saveButton.setIcon("fa-cog fa-spin");
    API.blog.save_article(article, function () {
        saveButton.setIcon("fa-check");
        setTimeout(function () {
            saveButton.setIcon("fa-save");
        }, 2000);
    });
}

function onSaveButtonInit(saveButton) {
    $(document).keydown(function (e) {
        if ((e.which == '115' || e.which == '83' ) && (e.ctrlKey || e.metaKey)) {
            e.stopPropagation();
            onSaveButtonClick(saveButton);
            return false;
        }
        return true;
    });
}

function onPublishButtonClick(publishButton) {
    function updateButtonFromStatus(success, published) {
        if (success === true) {
            publishButton.setIcon("fa-check");
            publishButton.setText(published ? "Published" : "Unpublished");
            article.published = published;
        } else {
            publishButton.setIcon("fa-question");
            publishButton.setText("Failed");
        }
        setTimeout(function () {
            updatePublishButton(publishButton, article.published);
        }, 2000);
    }

    publishButton.setIcon("fa-cog fa-spin");

    var apiFunction = article.published ? API.blog.unpublish_article : API.blog.publish_article;
    var postActionPublishStatus = article.published ? false : true;

    apiFunction(article.id, function (response, status) {
        updateButtonFromStatus(status.success, postActionPublishStatus);

        if (status.success !== true) {
            console.log(article.published ? "Unpublish" : "Publish" + " failed for reason: " + status.reason);
        }
    });
}

function updatePublishButton(publishButton, published) {
    if (published) {
        publishButton.setText("Unpublish");
        publishButton.setIcon("fa-cloud-download");
        publishButton.setClass("blue margin-left");
    } else {
        publishButton.setText("Publish");
        publishButton.setIcon("fa-cloud-upload");
        publishButton.setClass("green margin-left");
    }
}
function onPublishButtonInit(publishButton) {
    API.blog.get_article(article.id, function (response) {
        article.published = response['published'];
        updatePublishButton(publishButton, response['published']);
    });
}

var actions = [
    {
        name: "Save",
        icon: "fa-save",
        onClick: onSaveButtonClick,
        onInit: onSaveButtonInit
    },
    {
        name: "Delete",
        icon: "fa-trash",
        class: "red",
        onClick: onDeleteButtonClick
    },
    {
        name: "Publish",
        icon: "fa-cloud-download",
        text: "Unpublish",
        class: "green margin-left",
        onClick: onPublishButtonClick,
        onInit: onPublishButtonInit
    }
];

ReactDOM.render(React.createElement(ActionBar, {actions: actions}), $('.buttons-wrap')[0]);

},{"../../admin/components/ActionBar.jsx":1,"../../admin/components/Menu.jsx":2,"../../components/Dialog.jsx":7,"../../libs/Popover.js":10,"../components/ArticleEditor.jsx":3,"../components/ArticlePreview.jsx":4,"../components/ArticleWriter.jsx":5}],7:[function(require,module,exports){
Dialog = React.createClass({displayName: "Dialog",
    render: function () {
        return (
            React.createElement("div", {className: "dialog-component"}, 
                React.createElement("h1", null, this.props.title), 
                React.createElement("span", {className: "message"}, this.props.message), 
                React.createElement("div", {className: "buttons"}, 
                    React.createElement("div", {className: "button red", onClick: this.props.onDelete}, "Delete"), 
                    React.createElement("div", {className: "button blue", onClick: this.props.onCancel}, "Cancel")
                )
            )
        );
    }
});

},{}],8:[function(require,module,exports){
GIFPlayer = function () {
    $("img.gif").each(function (index, element) {
        var previewFrame = element.src;
        var actualGif = element.src.substr(0, element.src.length - 12) + ".gif";
        var e = $(element);

        var tmpImg = $(new Image());
        tmpImg.attr("src", e.attr('src'));
        tmpImg.on("load", function () {
            var overlay = e.next();
            overlay.width(e.width());
            overlay.height(e.height());
            overlay.css("margin-top", -overlay.height());

        });

        e.on("click", function () {
            console.log("click");
            if (e.attr("src") === previewFrame) {
                e.attr("src", actualGif);
                e.addClass("playing");
                e.removeClass("paused");
            } else {
                e.attr("src", previewFrame);
                e.addClass("paused");
                e.removeClass("playing");
            }
        });
    });
};

},{}],9:[function(require,module,exports){
//noinspection JSUnusedAssignmentre
API = {};

var ImageFormDescriptor = {
    'name': 'Image',
    'fields': [
        {
            'title': "Id",
            'name': "id",
            'type': "Hidden",
            'description': "The id of the image"
        },
        {
            'title': 'Title',
            'name': 'title',
            'type': 'Text',
            'description': 'The title of the image'
        },
        {
            'title': 'Description',
            'name': 'description',
            'type': 'Text',
            'description': 'Short description'
        },
        {
            'title': 'Banner Candidate',
            'name': 'bannerCandidate',
            'type': 'Boolean',
            'description': 'Can be used as a banner on the main page'
        },
        {
            'title': 'Gallery Item',
            'name': 'galleryItem',
            'type': 'Boolean',
            'description': 'Is visible on the gallery page'
        },
        {
            'title': 'Image Type',
            'name': 'imageType',
            'type': 'Choice',
            'choices': ['Sketch', 'Artwork', 'Screenshot'],
            'description': 'The category of the image'
        }
    ]
};

//noinspection JSUnusedGlobalSymbols
API.blog = {
    all: function (callback) {
        console.trace("[API] blog.all()");

        var url = "/api/articles/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    get_article: function (id, callback) {
        console.trace("[API] blog.get_article(" + id + ")");

        var url = "/api/articles/get/" + id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    add_tags: function (article_id, tags, callback) {
        console.trace("[API] blog.add_tags(" + article_id + ", " + tags + ")");

        var url = "/blog/tags/add/" + article_id + "/" + tags;
        $.post(url, function (data) {
            callback(data);
        });
    },

    remove_tag: function (article_id, tag, callback) {
        console.trace("[API] blog.remove_tag(" + article_id + ", " + tags + ")");

        tag = $.trim(tag);
        var url = "/blog/tags/remove/" + article_id + "/" + tag;
        $.post(url, function (data) {
            callback(data);
        });
    },

    get_tags: function (article_id, callback) {
        console.trace("[API] blog.get_tags(" + article_id + ")");

        var url = "/blog/tags/get/" + article_id;
        $.post(url, function (data) {
            callback(data);
        });
    },

    markdown: function (text, callback) {
        //console.trace("[API] blog.markdown("+text+")");

        var url = "/api/articles/markdown/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: text,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'text/plain; charset=UTF-8'
        });
    },

    save_article: function (article, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article);

        var url = "/api/articles/save/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify(article),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_article: function (article_id, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article_id);

        var url = "/api/articles/delete/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    publish_article: function (article_id, callback) {
        console.trace("[API] blog.publish_article()");
        console.trace(article_id);

        var url = "/api/articles/publish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    unpublish_article: function (article_id, callback) {
        console.trace("[API] blog.unpublish_article()");
        console.trace(article_id);

        var url = "/api/articles/unpublish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};

API.gallery = {
    all: function (callback) {
        var url = "/api/gallery/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    image_descriptor: function (callback) {
        callback(ImageFormDescriptor);
    },

    get_image: function (image_id, callback) {
        var url = "/api/gallery/image/get/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    update_image: function (image, callback) {
        image['json'] = true;

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: "/api/gallery/image/update/" + image['id'],
            data: JSON.stringify(image),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_image: function (image_id, callback) {
        var url = "/api/gallery/image/delete/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    upload_image: function (file, onSuccess, onProgress) {
        var url = "/api/gallery/upload";
        var file_data = new FormData();
        file_data.append("file", file);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = event.loaded / event.total;
                        percentComplete = parseInt(percentComplete * 100);
                        onProgress(percentComplete);
                    }
                }, false);

                return xhr;
            },
            url: url,
            type: "POST",
            data: file_data,
            processData: false,
            contentType: false,
            success: function (response) {
                onSuccess(response);
            }
        });
    }
};

API.auth = {
    login: function (username, password, callback) {
        console.trace("[API] auth.login(username, password)");
        console.trace(username);

        var url = "/api/auth/login";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'username': username, 'password': password}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    logged_user: function (callback) {
        console.trace("[API] auth.logged_user()");

        var url = "/api/auth/logged_user";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};
},{}],10:[function(require,module,exports){
Popover = new (function () {
    var CONTAINER_ID = "ktzPopoverContainer";

    var ELEMENT_REACT = 0;
    var ELEMENT_DOM = 1;
    var ELEMENT_NONE = 2;

    var install = $.proxy(function () {
        if (isInstalled()) {
            return;
        }

        var body = $("body");
        body.append(this.cover);
        body.keydown(function (event) {
            if (event.which == 27) {
                Popover.hide();
            }
        });

        console.log("Popover installed");
    }, this);

    var isInstalled = function () {
        return $("#" + CONTAINER_ID).length == 1;
    };

    var removeAnimationClasses = $.proxy(function () {
        var container = this.container;
        container.classList().forEach(function (element) {
            if (element.startsWith("animation-")) {
                container.removeClass(element);
            }
        });
    }, this);

    var animateShow = $.proxy(function (animationClass) {
        animationClass = animationClass || "default";
        removeAnimationClasses();
        this.container.addClass("animation-show-" + animationClass);

        this.cover.css("visibility", "visible");
        this.cover.css("opacity", "0");
        this.cover.animate({
            opacity: 1.0
        }, 100);
    }, this);

    var animateHide = $.proxy(function (animation) {
        removeAnimationClasses();
        animation = animation || "default";
        this.container.addClass("animation-hide-" + animation);

        // animate opacity to 0 then set visibility to hidden
        this.cover.animate({
                opacity: .0
            }, 100, $.proxy(function () {
                this.cover.css("visibility", "hidden");
            }, this)
        );
    }, this);

    this.showElement = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountDomElement(element);
        animateShow(animation);
    };

    this.showReact = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountReact(element);
        animateShow(animation);
    };

    this.hide = function () {
        animateHide(this.animation);
    };

    var mountReact = $.proxy(function (element) {
        ReactDOM.render(element, this.container[0]);
        this.elementType = ELEMENT_REACT;
    }, this);

    var unmountReact = $.proxy(function () {
        ReactDOM.unmountComponentAtNode(this.container[0]);
        this.elementType = ELEMENT_NONE;
    }, this);

    var mountDomElement = $.proxy(function (element) {
        this.clientElement = element;
        this.container.append(this.clientElement);
        this.elementType = ELEMENT_DOM;
    }, this);

    var unmountDomElement = $.proxy(function () {
        if (this.clientElement !== undefined) {
            this.clientElement.detach();
        }
        this.elementType = ELEMENT_NONE;
    }, this);

    var unmountClient = $.proxy(function () {
        if (this.elementType == ELEMENT_DOM) {
            unmountDomElement();
        }
        if (this.elementType == ELEMENT_REACT) {
            unmountReact();
        }
    }, this);

    // Constructor
    /**
     * Container for the actual popover contents
     */
    this.container = $("<div></div>");
    this.container.addClass("content");

    /**
     * The cover that hides the underlying page
     */
    this.cover = $("<div>");
    this.cover.addClass("modal-popup");
    this.cover.attr("id", CONTAINER_ID);
    this.cover.css("opacity", "0");

    this.cover.append(this.container);

    install();
})();
},{}]},{},[6]);
