(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
GIFPlayer = function () {
    $("img.gif").each(function (index, element) {
        var previewFrame = element.src;
        var actualGif = element.src.substr(0, element.src.length - 12) + ".gif";
        var e = $(element);

        var tmpImg = $(new Image());
        tmpImg.attr("src", e.attr('src'));
        tmpImg.on("load", function () {
            var overlay = e.next();
            overlay.width(e.width());
            overlay.height(e.height());
            overlay.css("margin-top", -overlay.height());

        });

        e.on("click", function () {
            console.log("click");
            if (e.attr("src") === previewFrame) {
                e.attr("src", actualGif);
                e.addClass("playing");
                e.removeClass("paused");
            } else {
                e.attr("src", previewFrame);
                e.addClass("paused");
                e.removeClass("playing");
            }
        });
    });
};

},{}]},{},[1]);
