(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
Dialog = React.createClass({displayName: "Dialog",
    render: function () {
        return (
            React.createElement("div", {className: "dialog-component"}, 
                React.createElement("h1", null, this.props.title), 
                React.createElement("span", {className: "message"}, this.props.message), 
                React.createElement("div", {className: "buttons"}, 
                    React.createElement("div", {className: "button red", onClick: this.props.onDelete}, "Delete"), 
                    React.createElement("div", {className: "button blue", onClick: this.props.onCancel}, "Cancel")
                )
            )
        );
    }
});

},{}],2:[function(require,module,exports){
require("../../components/Dialog.jsx");

require("../../libs/Popover.js");
require("../../libs/Forms.js");
require("../../libs/API.js");
require("../../libs/std.js");

Image = React.createClass({displayName: "Image",

    componentDidMount: function () {
        var imageId = this.props.image.id;

        this.deleteDialog = React.createElement(Dialog, {
            title: "Confirmation",
            message: "Are you sure you want to permanently remove this image?",
            onDelete: function () {
                API.gallery.delete_image(imageId, function (response, status) {
                    if (status.success) {
                        Popover.hide();
                    } else {
                        console.log(status);
                    }
                });
            },
            onCancel: function () {
                Popover.hide();
            }
        });

        API.gallery.image_descriptor($.proxy(function (data) {
            this.form = new Form(data);
            this.form.setTitle("Edit Image");
            this.form.onCancel(function () {
                Popover.hide();
            });
            this.form.onApply($.proxy(function () {
                API.gallery.update_image(this.form.getValue(), $.proxy(function (response, status) {
                    if (status["success"]) {
                        Popover.hide();
                    } else {
                        this.form.setErrors(response.errors);
                    }
                }, this));
            }, this));
        }, this));
    },

    onEdit: function (event) {
        API.gallery.get_image(this.props.image.id, $.proxy(function (data) {
            this.form.setValue(data);
            this.form.clearErrors();
            Popover.showElement(this.form.element);
        }, this));
        event.stopPropagation();
    },

    onDelete: function (event) {
        Popover.showReact(this.deleteDialog);
        event.stopPropagation();
    },

    onCopy: function () {
        event.stopPropagation();
    },

    onPreview: function () {
        Popover.showReact(React.createElement(ImagePreview, {imageSource: this.props.image.src}), "scale");
    },

    render: function () {
        var headerFlag = '';
        if (this.props.image.banner_candidate) {
            headerFlag = React.createElement("div", {className: "header-flag", title: "This image is a header candidate"}, "header")
        }
        return (
            React.createElement("div", {className: "item"}, 
                React.createElement("div", {className: "thumbnail", style: {'backgroundImage': 'url(' + this.props.image.src + ')'}, 
                     onClick: this.onPreview}, 
                    React.createElement("div", {className: "edit-wrap"}, React.createElement("i", {className: "fa fa-trash-o red", onClick: this.onDelete})), 
                    React.createElement("div", {className: "edit-wrap"}, React.createElement("i", {className: "fa fa-pencil blue", onClick: this.onEdit})), 
                    React.createElement("a", {href: this.props.image.src}, 
                        React.createElement("div", {className: "edit-wrap"}, React.createElement("i", {className: "fa fa-link blue", onClick: this.onCopy}))
                    ), 
                    React.createElement("div", {className: "info group"}, 
                        React.createElement("span", {className: "title"}, this.props.image.title), React.createElement("br", null), 
                        React.createElement("span", {className: "details"}, this.props.image.w, " x ", this.props.image.h, 
                            ", ", humanizeMemorySize(this.props.image.size)), 
                        headerFlag
                    )
                )
            ));
    }
});

},{"../../components/Dialog.jsx":1,"../../libs/API.js":3,"../../libs/Forms.js":4,"../../libs/Popover.js":5,"../../libs/std.js":6}],3:[function(require,module,exports){
//noinspection JSUnusedAssignmentre
API = {};

var ImageFormDescriptor = {
    'name': 'Image',
    'fields': [
        {
            'title': "Id",
            'name': "id",
            'type': "Hidden",
            'description': "The id of the image"
        },
        {
            'title': 'Title',
            'name': 'title',
            'type': 'Text',
            'description': 'The title of the image'
        },
        {
            'title': 'Description',
            'name': 'description',
            'type': 'Text',
            'description': 'Short description'
        },
        {
            'title': 'Banner Candidate',
            'name': 'bannerCandidate',
            'type': 'Boolean',
            'description': 'Can be used as a banner on the main page'
        },
        {
            'title': 'Gallery Item',
            'name': 'galleryItem',
            'type': 'Boolean',
            'description': 'Is visible on the gallery page'
        },
        {
            'title': 'Image Type',
            'name': 'imageType',
            'type': 'Choice',
            'choices': ['Sketch', 'Artwork', 'Screenshot'],
            'description': 'The category of the image'
        }
    ]
};

//noinspection JSUnusedGlobalSymbols
API.blog = {
    all: function (callback) {
        console.trace("[API] blog.all()");

        var url = "/api/articles/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    get_article: function (id, callback) {
        console.trace("[API] blog.get_article(" + id + ")");

        var url = "/api/articles/get/" + id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    add_tags: function (article_id, tags, callback) {
        console.trace("[API] blog.add_tags(" + article_id + ", " + tags + ")");

        var url = "/blog/tags/add/" + article_id + "/" + tags;
        $.post(url, function (data) {
            callback(data);
        });
    },

    remove_tag: function (article_id, tag, callback) {
        console.trace("[API] blog.remove_tag(" + article_id + ", " + tags + ")");

        tag = $.trim(tag);
        var url = "/blog/tags/remove/" + article_id + "/" + tag;
        $.post(url, function (data) {
            callback(data);
        });
    },

    get_tags: function (article_id, callback) {
        console.trace("[API] blog.get_tags(" + article_id + ")");

        var url = "/blog/tags/get/" + article_id;
        $.post(url, function (data) {
            callback(data);
        });
    },

    markdown: function (text, callback) {
        //console.trace("[API] blog.markdown("+text+")");

        var url = "/api/articles/markdown/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: text,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'text/plain; charset=UTF-8'
        });
    },

    save_article: function (article, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article);

        var url = "/api/articles/save/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify(article),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_article: function (article_id, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article_id);

        var url = "/api/articles/delete/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    publish_article: function (article_id, callback) {
        console.trace("[API] blog.publish_article()");
        console.trace(article_id);

        var url = "/api/articles/publish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    unpublish_article: function (article_id, callback) {
        console.trace("[API] blog.unpublish_article()");
        console.trace(article_id);

        var url = "/api/articles/unpublish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};

API.gallery = {
    all: function (callback) {
        var url = "/api/gallery/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    image_descriptor: function (callback) {
        callback(ImageFormDescriptor);
    },

    get_image: function (image_id, callback) {
        var url = "/api/gallery/image/get/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    update_image: function (image, callback) {
        image['json'] = true;

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: "/api/gallery/image/update/" + image['id'],
            data: JSON.stringify(image),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_image: function (image_id, callback) {
        var url = "/api/gallery/image/delete/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    upload_image: function (file, onSuccess, onProgress) {
        var url = "/api/gallery/upload";
        var file_data = new FormData();
        file_data.append("file", file);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = event.loaded / event.total;
                        percentComplete = parseInt(percentComplete * 100);
                        onProgress(percentComplete);
                    }
                }, false);

                return xhr;
            },
            url: url,
            type: "POST",
            data: file_data,
            processData: false,
            contentType: false,
            success: function (response) {
                onSuccess(response);
            }
        });
    }
};

API.auth = {
    login: function (username, password, callback) {
        console.trace("[API] auth.login(username, password)");
        console.trace(username);

        var url = "/api/auth/login";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'username': username, 'password': password}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    logged_user: function (callback) {
        console.trace("[API] auth.logged_user()");

        var url = "/api/auth/logged_user";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};
},{}],4:[function(require,module,exports){
uuid4 = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        //noinspection JSUnresolvedFunction,JSUnresolvedVariable
        var r = crypto.getRandomValues(new Uint8Array(1))[0] % 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

isEnterKey = function (key) {
    return key == 13;
};

Form = function (modelDescriptor) {

    this.init = function () {
        this.id = uuid4();

        this.fields = [];

        this.descriptor = modelDescriptor;

        this.applyButton = $('<a>').addClass("button").addClass("blue").html("Save");
        this.cancelButton = $("<a>").addClass("button").addClass("red").html("Cancel");

        modelDescriptor['fields'].forEach($.proxy(function (fieldDescriptor) {
            var field = this.createField(fieldDescriptor);
            this.appendField(field);
        }, this));

        this.render();
    };

    this.setTitle = function (title) {
        this.header.html(title);
    };

    this.createField = function (fieldDescriptor) {
        switch (fieldDescriptor.type) {
            case "Text":
                var textField = new TextField(fieldDescriptor);
                textField.editor.keypress($.proxy(function (event) {
                    if (isEnterKey(event.which)) {
                        this.applyButton.click();
                        event.preventDefault();
                    }
                }, this));
                return textField;
            case "Boolean":
                return new BooleanField(fieldDescriptor);
            case "Choice":
                return new ChoiceField(fieldDescriptor);
            case "Hidden":
                return new HiddenField(fieldDescriptor);
        }
    };

    this.appendField = function (field) {
        this.fields.push(field);
    };

    this.render = function () {
        this.element = $("<div>")
            .attr("id", this.id)
            .addClass("form")
            .addClass("dialog");

        this.header = $("<h1>")
            .html(this.descriptor['title']);

        this.errors = $("<div>").addClass("errors").hide();

        var buttonWrap = $("<div>")
            .addClass("buttons")
            .append(this.applyButton)
            .append(this.cancelButton);

        this.element.append(this.header);

        this.element.append(this.errors);

        this.fields.forEach($.proxy(function (field) {
            this.element.append(field.element);
        }, this));

        this.element.append(buttonWrap);

        return this.element;
    };

    this.setValue = function (object) {
        this.fields.forEach(function (field) {
            field.setValue(object[field.descriptor['name']]);
        });
    };

    this.getValue = function () {
        var object = {};
        this.fields.forEach(function (field) {
            object[field.descriptor['name']] = field.getValue();
        });
        return object;
    };

    this.onApply = function (callback) {
        this.applyButton.click(callback);
    };

    this.onCancel = function (callback) {
        this.cancelButton.click(callback);
    };

    this.setErrors = function (errors) {
        var errorList = $("<ul>");
        errors.forEach(function (error) {
            var errorElement = $("<li>").html(error);
            errorList.append(errorElement);
        });
        this.errors.append(errorList);
        this.errors.slideDown();
    };

    this.clearErrors = function () {
        this.errors.empty();
        this.errors.hide();
    };

    this.init();
};

/**
 * Field used to edit lines of text
 */
function TextField(fieldDescriptor) {

    this.init = function () {
        this.descriptor = fieldDescriptor;

        this.elementClass = "_field__" + this.descriptor['name'];

        this.element = $("<div>");

        this.editor = $("<input>")
            .addClass("text-field")
            .addClass(this.elementClass);

        var label = $("<div>")
            .addClass("label")
            .html(this.descriptor['title']);

        this.element.append(this.editor);
        this.element.append(label);
    };

    this.getValue = function () {
        return this.editor.val();
    };

    this.setValue = function (value) {
        this.editor.val(value);
    };

    this.init();
}

/**
 * Field used to edit boolean values
 */
function BooleanField(fieldDescriptor) {
    this.init = function () {
        this.descriptor = fieldDescriptor;

        this.elementClass = "_field__" + this.descriptor['name'].replace(" ", "");

        this.element = $("<div>");
        this.element.addClass("boolean-field");
        this.element.addClass(this.elementClass);

        var label = $('<div class="label">' + this.descriptor['title'] + '</div>');

        this.editor = $("<div>");
        this.editor.addClass("editor");
        this.editor.click($.proxy(function () {
            this.toggleValue();
        }, this));

        this.checkMarkWrap = $('<div class="check-mark-wrap"></div>');
        this.checkMark = $('<i class="fa fa-check check-mark off"></i>');

        this.checkMarkWrap.append(this.checkMark);

        var details = $('<span class="details">' + this.descriptor['description'] + '</span>');

        this.editor.append(this.checkMarkWrap);
        this.editor.append(details);

        this.element.append(label);
        this.element.append(this.editor);
    };

    this.getValue = function () {
        return this.checkMark.hasClass('on');
    };

    this.setValue = function (value) {
        if (value != this.getValue()) {
            this.checkMark.removeClass("on").removeClass("off").addClass(value ? "on" : "off");
        }
        this.element.value = value;
    };

    this.toggleValue = function () {
        this.setValue(!this.getValue());
    };

    this.init();
}

/**
 * Field used to edit enums
 */
function ChoiceField(fieldDescriptor) {
    this.init = function () {
        this.descriptor = fieldDescriptor;
        this.elementClass = "_field__" + this.descriptor['name'];


        this.element = $("<div>");
        this.element.addClass("select-field");
        this.element.addClass(this.elementClass);

        var title = $("<div>");
        title.addClass("title");
        title.html(this.descriptor['title']);
        this.element.append(title);

        var icon = $("<i>");
        icon.addClass("fa").addClass("fa-chevron-down");
        this.element.append(icon);

        this.editor = $("<select>");
        this.editor.addClass("editor");
        this.element.append(this.editor);

        this.descriptor['choices'].forEach($.proxy(function (item) {
            var option = $("<option>");
            option.attr("value", item);
            option.html(item);
            this.editor.append(option);
        }, this));
    };

    this.getValue = function () {
        return this.editor.value;
    };

    this.setValue = function (value) {
        if (this.descriptor['choices'].indexOf(value) >= 0) {
            this.editor.value = value;
        }
    };

    this.init();
}

function HiddenField(descriptor) {
    this.descriptor = descriptor;

    this.element = '';

    this.value = '';

    this.setValue = function (value) {
        this.value = value;
    };

    this.getValue = function () {
        return this.value;
    };

    this.render = function () {
        return "";
    }
}

},{}],5:[function(require,module,exports){
Popover = new (function () {
    var CONTAINER_ID = "ktzPopoverContainer";

    var ELEMENT_REACT = 0;
    var ELEMENT_DOM = 1;
    var ELEMENT_NONE = 2;

    var install = $.proxy(function () {
        if (isInstalled()) {
            return;
        }

        var body = $("body");
        body.append(this.cover);
        body.keydown(function (event) {
            if (event.which == 27) {
                Popover.hide();
            }
        });

        console.log("Popover installed");
    }, this);

    var isInstalled = function () {
        return $("#" + CONTAINER_ID).length == 1;
    };

    var removeAnimationClasses = $.proxy(function () {
        var container = this.container;
        container.classList().forEach(function (element) {
            if (element.startsWith("animation-")) {
                container.removeClass(element);
            }
        });
    }, this);

    var animateShow = $.proxy(function (animationClass) {
        animationClass = animationClass || "default";
        removeAnimationClasses();
        this.container.addClass("animation-show-" + animationClass);

        this.cover.css("visibility", "visible");
        this.cover.css("opacity", "0");
        this.cover.animate({
            opacity: 1.0
        }, 100);
    }, this);

    var animateHide = $.proxy(function (animation) {
        removeAnimationClasses();
        animation = animation || "default";
        this.container.addClass("animation-hide-" + animation);

        // animate opacity to 0 then set visibility to hidden
        this.cover.animate({
                opacity: .0
            }, 100, $.proxy(function () {
                this.cover.css("visibility", "hidden");
            }, this)
        );
    }, this);

    this.showElement = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountDomElement(element);
        animateShow(animation);
    };

    this.showReact = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountReact(element);
        animateShow(animation);
    };

    this.hide = function () {
        animateHide(this.animation);
    };

    var mountReact = $.proxy(function (element) {
        ReactDOM.render(element, this.container[0]);
        this.elementType = ELEMENT_REACT;
    }, this);

    var unmountReact = $.proxy(function () {
        ReactDOM.unmountComponentAtNode(this.container[0]);
        this.elementType = ELEMENT_NONE;
    }, this);

    var mountDomElement = $.proxy(function (element) {
        this.clientElement = element;
        this.container.append(this.clientElement);
        this.elementType = ELEMENT_DOM;
    }, this);

    var unmountDomElement = $.proxy(function () {
        if (this.clientElement !== undefined) {
            this.clientElement.detach();
        }
        this.elementType = ELEMENT_NONE;
    }, this);

    var unmountClient = $.proxy(function () {
        if (this.elementType == ELEMENT_DOM) {
            unmountDomElement();
        }
        if (this.elementType == ELEMENT_REACT) {
            unmountReact();
        }
    }, this);

    // Constructor
    /**
     * Container for the actual popover contents
     */
    this.container = $("<div></div>");
    this.container.addClass("content");

    /**
     * The cover that hides the underlying page
     */
    this.cover = $("<div>");
    this.cover.addClass("modal-popup");
    this.cover.attr("id", CONTAINER_ID);
    this.cover.css("opacity", "0");

    this.cover.append(this.container);

    install();
})();
},{}],6:[function(require,module,exports){
/**
 * Created by Alex on 30/11/2015.
 */

humanizeMemorySize = function humanizeMemorySize(bytes) {
    bytes = parseInt(bytes);
    var unitMultiples = ["b", "Kb", "Mb", "Gb", "Tb"];
    var unit = 0;
    while (bytes > 1000) {
        unit += 1;
        bytes /= 1000;
    }
    return parseInt(bytes * 100) / 100 + unitMultiples[unit];
};
},{}]},{},[2]);
