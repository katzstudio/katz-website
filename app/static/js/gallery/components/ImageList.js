(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
require("../../libs/API.js");
require("../../libs/Matcher.js");


ImageListSearch = {};

ImageList = React.createClass({displayName: "ImageList",
    getInitialState: function () {
        return {data: [], matcher: EmptyMatcher};
    },

    fetchData: function () {
        var that = this;
        API.gallery.all(function (response) {
            that.setState({images: response});
        });
    },

    componentDidMount: function () {
        ImageListSearch.search = $.proxy(function (query) {
            this.state.matcher = new Matcher(query);
        }, this);

        ImageListSearch.clearSearch = $.proxy(function (query) {
            this.state.matcher = EmptyMatcher;
        }, this);

        this.fetchData();
        setInterval(this.fetchData, this.props.pollInterval);
    },

    componentWillUnmount: function () {
        clearInterval(this.fetchData);

        ImageListSearch.search = undefined;
        ImageListSearch.clearSearch = undefined;
    },

    hasImages: function () {
        return this.state.images !== undefined;
    },

    render: function () {
        var searchFilter = '';
        var hasFilter = false;
        if (this.state.matcher !== EmptyMatcher) {
            searchFilter =
                React.createElement("div", {className: "filter"}, "Only showing images with name: ", React.createElement("span", {
                    className: "value"}, this.state.matcher.query), 
                    React.createElement("i", {className: "fa fa-close", onClick: ImageListSearch.clearSearch})
                );
        }

        var imageNodes = "";
        if (this.hasImages()) {
            imageNodes = this.state.images.map($.proxy(function (image) {
                if (this.state.matcher.matches(image.title)) {
                    return (React.createElement(Image, {image: image, key: image.src}));
                } else {
                    return '';
                }

            }, this));
        }

        return (
            React.createElement("div", {className: "full-width gallery-admin group"}, 
                searchFilter, " ", React.createElement("br", null), 
                imageNodes
            )
        );
    }
});
},{"../../libs/API.js":2,"../../libs/Matcher.js":3}],2:[function(require,module,exports){
//noinspection JSUnusedAssignmentre
API = {};

//noinspection JSUnusedGlobalSymbols
API.blog = {
    all: function (callback) {
        var url = "/api/articles/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    add_tags: function (article_id, tags, callback) {
        var url = "/blog/tags/add/" + article_id + "/" + tags;
        $.post(url, function (data) {
            callback(data);
        });
    },

    remove_tag: function (article_id, tag, callback) {
        tag = $.trim(tag);
        var url = "/blog/tags/remove/" + article_id + "/" + tag;
        $.post(url, function (data) {
            callback(data);
        });
    },

    get_tags: function (article_id, callback) {
        var url = "/blog/tags/get/" + article_id;
        $.post(url, function (data) {
            callback(data);
        });
    },

    markdown: function(text, callback) {
        var url = "/api/articles/markdown/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: text,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'text/plain; charset=UTF-8'
        });
    }
};

API.gallery = {
    all: function (callback) {
        var url = "/api/gallery/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    image_descriptor: function (callback) {
        var url = "/api/gallery/image/descriptor";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    get_image: function (image_id, callback) {
        var url = "/api/gallery/image/get/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    update_image: function (image, callback) {
        image['json'] = true;

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: "/api/gallery/image/update/" + image['id'],
            data: JSON.stringify(image),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_image: function (image_id, callback) {
        var url = "/api/gallery/image/delete/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    upload_image: function (file, onSuccess, onProgress) {
        var url = "/api/gallery/upload";
        var file_data = new FormData();
        file_data.append("file", file);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = event.loaded / event.total;
                        percentComplete = parseInt(percentComplete * 100);
                        onProgress(percentComplete);
                    }
                }, false);

                return xhr;
            },
            url: url,
            type: "POST",
            data: file_data,
            processData: false,
            contentType: false,
            success: function (response) {
                onSuccess(response);
            }
        });
    }
};

},{}],3:[function(require,module,exports){
/**
 * Creates a Matcher object used to apply searcth queries over items or arrays of items.
 *
 * @param query a user search query
 */
Matcher = function (query) {

    // the search query is split into lowercase words
    this.words = query.split(' ').map(function (word) {
        return word.toLowerCase();
    });

    this.query = query;

    /**
     * Tests a single value against a matcher
     * @param matcher the matcher used to test the value against
     * @param value the requested value
     * @returns {*} true if the matcher matches the value. false otherwise
     */
    function matchesSingleValue(matcher, value) {
        if (value.constructor != String) {
            return false;
        }

        var partialMatches = matcher.words.map(function (word) {
            return value.toLocaleLowerCase().includes(word);
        });

        return partialMatches.reduce(function (acc, current) {
            return acc && current;
        }, true);
    }

    /**
     * Tests a single value or an array of values against a matcher
     * @param value either one String or an Array of Strings to be matched by the matcher
     * @returns {*} either true or false if a single value was matched by the string, or an array of match results
     */

    this.matches = function (value) {
        var thisMatcher = this;
        if (value.constructor == String) {
            return matchesSingleValue(thisMatcher, value);
        }

        if (value.constructor == Array) {
            return value.map(function (item) {
                return matchesSingleValue(thisMatcher, item);
            });
        }

        return false;
    }
};

/**
 * Empty matcher that matches ANYTHING
 * @type {Matcher}
 */
EmptyMatcher = new Matcher('');

},{}]},{},[1]);
