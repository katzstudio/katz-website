(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
Action = React.createClass({displayName: "Action",
    getInitialState: function () {
        return {
            'icon': this.props.action.icon,
            'text': this.props.action.text,
            'class': this.props.action.class !== undefined ? this.props.action.class : '',
            'href': this.props.action.href
        }
    },

    setText: function (text) {
        this.setState({text: text});
    },

    setIcon: function (icon) {
        this.setState({icon: icon});
    },

    setClass: function (buttonClass) {
        this.setState({class: buttonClass});
    },

    onClick: function (event) {
        this.props.action.onClick(this);
    },

    componentDidMount: function () {
        if (this.props.action.onInit !== undefined) {
            this.props.action.onInit(this);
        }
    },

    render: function () {
        var iconClass = "fa " + this.state.icon;
        var textComponent = undefined;
        if (this.props.action.text !== undefined && this.state.text != "") {
            textComponent = React.createElement("span", {ref: "text"}, this.state.text);
        }

        var buttonClass = "button " + this.state.class;

        var button = (
            React.createElement("div", {className: buttonClass, onClick: this.onClick}, 
                React.createElement("i", {className: iconClass, ref: "icon"}), 
                textComponent
            ));

        if (this.props.action.href !== undefined) {
            return React.createElement("a", {href: this.props.action.href}, button)
        } else {
            return button;
        }
    }
});

ActionBar = React.createClass({displayName: "ActionBar",
    render: function () {
        var buttons = this.props.actions.map(function (action, index) {
            return (React.createElement(Action, {action: action, key: index}));
        });

        return (
            React.createElement("div", {className: "buttons"}, 
                buttons
            )
        );
    }
});

},{}],2:[function(require,module,exports){
Menu = React.createClass({displayName: "Menu",
    getDefaultProps: function () {
        return {
            items: [
                {name: "Dashboard", icon: "fa-dashboard", url: "#", disabled: true},
                {name: "Blog", icon: "fa-pencil", url: "/admin/articles"},
                {name: "Gallery", icon: "fa-camera", url: "/admin/gallery"},
                {name: "Users", icon: "fa-user", url: "#", disabled: true},
                {name: "Settings", icon: "fa-cog", url: "#", disabled: true},
                {name: "Messages", icon: "fa-envelope", url: "#", disabled: true}
            ]
        };
    },

    render: function () {
        var activeItem = this.props.active;
        var items = this.props.items.map(function (item) {
            var iconClass = "fa " + item.icon;
            var wrapperClass = "item";
            if (item.name === activeItem) {
                wrapperClass += " active";
            }
            if (item.disabled) {
                wrapperClass += " disabled";
            }

            return (React.createElement("a", {href: item.url, key: item.name}, React.createElement("div", {className: wrapperClass}, 
                React.createElement("i", {className: iconClass})
            )));
        });
        return (React.createElement("div", null, items));
    }
});

},{}],3:[function(require,module,exports){
SearchBar = React.createClass({displayName: "SearchBar",
    onSearch: function (value) {
        if (this.props.onSearch !== undefined) {
            this.props.onSearch(value);
        }
    },

    onKeyDown: function (event) {
        if (event.which == 13) {
            var input = $(this.refs.input);
            var search = $(this.refs.search);
            if (input.val() != "") {
                this.props.onSearch(input.val());
                input.val("");
            }
            search.removeClass("extended");
            $(input).blur();
        }
    },

    onClick: function () {
        var search = $(this.refs.search);
        var input = $(this.refs.input);
        if (!search.hasClass("extended")) {
            search.addClass("extended");
            input.focus();
        }
    },

    onBlur: function () {
        $(this.refs.search).removeClass("extended");
    },

    render: function () {
        return (
            React.createElement("div", {className: "search", ref: "search", onClick: this.onClick}, 
                React.createElement("i", {className: "fa fa-search"}), 
                React.createElement("input", {ref: "input", spellCheck: "false", onKeyDown: this.onKeyDown, onBlur: this.onBlur})
            )
        );
    }
});

},{}],4:[function(require,module,exports){
Dialog = React.createClass({displayName: "Dialog",
    render: function () {
        return (
            React.createElement("div", {className: "dialog-component"}, 
                React.createElement("h1", null, this.props.title), 
                React.createElement("span", {className: "message"}, this.props.message), 
                React.createElement("div", {className: "buttons"}, 
                    React.createElement("div", {className: "button red", onClick: this.props.onDelete}, "Delete"), 
                    React.createElement("div", {className: "button blue", onClick: this.props.onCancel}, "Cancel")
                )
            )
        );
    }
});

},{}],5:[function(require,module,exports){
UploadLoadingBar = React.createClass({displayName: "UploadLoadingBar",
    getInitialState: function () {
        return {
            filename: 'Forest_01.jpeg',
            percent: 0,
            file_size: "5Mb"
        }
    },

    onUploadAnother: function () {
        if (this.props.onUploadAnother !== undefined) {
            this.props.onUploadAnother();
        }
    },

    onClose: function () {
        Popover.hide();
    },

    render: function () {
        var percent = this.props.status.percent;
        var totalSize = this.props.status.file_size;
        var uploaded = (totalSize * percent) / 100;

        var filenameInfo;
        if (this.props.status.percent < 100) {
            filenameInfo = "Uploading " + this.props.status.filename;
        } else {
            filenameInfo = "Uploaded " + this.props.status.filename;
        }

        var progressInfo;
        if (this.props.status.percent < 100) {
            progressInfo = humanizeMemorySize(uploaded) + " / " + humanizeMemorySize(totalSize);
        } else {
            progressInfo = humanizeMemorySize(totalSize);
        }

        var loadingFillStyle = {
            width: this.props.status.percent + "%"
        };

        var buttonsStyle = {
            transition: "0.3s",
            height: percent == 100 ? "56px" : "0px",
            overflow: "hidden",
            paddingTop: "10px"
        };

        return (
            React.createElement("div", {className: "upload-component"}, 
                React.createElement("h1", null, "Upload"), 
                React.createElement("div", {className: "progress-bar"}, 
                    React.createElement("div", {className: "info-wrap"}, 
                        React.createElement("div", {className: "percent"},  percent, "%"), 
                        React.createElement("div", {className: "info"}, 
                            React.createElement("span", {className: "first-line"}, filenameInfo), 
                            React.createElement("span", {className: "second-line"}, progressInfo)
                        )

                    ), 
                    React.createElement("div", {className: "fill", style: loadingFillStyle}
                    )
                ), 
                React.createElement("div", {className: "buttons", style: buttonsStyle}, 
                    React.createElement("div", {className: "button blue", onClick: this.onUploadAnother}, "Another One"), 
                    React.createElement("div", {className: "button red", onClick: this.onClose}, "Close")
                )
            )
        );
    }
});

UploadDropzone = React.createClass({displayName: "UploadDropzone",
    waitingMessage: "Drop image here to upload",

    hoveredMessage: "Drop it like it's hot!",

    getInitialState: function () {
        return {hovered: false};
    },

    componentDidMount: function () {
        var dropzoneAndChildren = $(".dropzone *");

        dropzoneAndChildren.bind('dragenter', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: true});
        }, this));

        dropzoneAndChildren.bind('dragover', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();
        }, this));

        dropzoneAndChildren.bind('dragleave', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: false});
        }, this));

        dropzoneAndChildren.bind('drop', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: false});
            this.props.onDrop(event);
        }, this))
    },

    render: function () {
        var dropzoneClass = 'dropzone ' + (this.state.hovered ? 'active' : '');
        var message = this.state.hovered ? this.hoveredMessage : this.waitingMessage;
        return (
            React.createElement("div", {className: "upload-component waiting"}, 
                React.createElement("div", {className: dropzoneClass}, 
                    React.createElement("span", {className: "message"}, message)
                )
            )
        );
    }
});

Upload = React.createClass({displayName: "Upload",
    getInitialState: function () {
        return {
            uploading: false
        };
    },

    onUploadSuccess: function (response) {
        console.log("onUploadSuccess " + response);
    },

    onUploadProgress: function (progress) {
        console.log("onUploadProgress " + progress);
        this.setState({progress: progress});
    },

    onDrop: function (event) {
        var file = event.originalEvent.dataTransfer.files[0];
        API.gallery.upload_image(file, this.onUploadSuccess, this.onUploadProgress);
        this.setState({uploading: true, progress: 0, file: file});
    },

    onUploadAnother: function () {
        this.setState({uploading: false});
    },

    render: function () {
        if (this.state.uploading) {
            var uploadStatus = {
                filename: this.state.file.name,
                percent: this.state.progress,
                file_size: this.state.file.size
            };
            return (React.createElement(UploadLoadingBar, {status: uploadStatus, onUploadAnother: this.onUploadAnother}));
        } else {
            return (React.createElement(UploadDropzone, {onDrop: this.onDrop}));
        }
    }
})
;
},{}],6:[function(require,module,exports){
require("../../components/Dialog.jsx");

require("../../libs/Popover.js");
require("../../libs/Forms.js");
require("../../libs/API.js");
require("../../libs/std.js");

Image = React.createClass({displayName: "Image",

    componentDidMount: function () {
        var imageId = this.props.image.id;

        this.deleteDialog = React.createElement(Dialog, {
            title: "Confirmation",
            message: "Are you sure you want to permanently remove this image?",
            onDelete: function () {
                API.gallery.delete_image(imageId, function (response, status) {
                    if (status.success) {
                        Popover.hide();
                    } else {
                        console.log(status);
                    }
                });
            },
            onCancel: function () {
                Popover.hide();
            }
        });

        API.gallery.image_descriptor($.proxy(function (data) {
            this.form = new Form(data);
            this.form.setTitle("Edit Image");
            this.form.onCancel(function () {
                Popover.hide();
            });
            this.form.onApply($.proxy(function () {
                API.gallery.update_image(this.form.getValue(), $.proxy(function (response, status) {
                    if (status["success"]) {
                        Popover.hide();
                    } else {
                        this.form.setErrors(response.errors);
                    }
                }, this));
            }, this));
        }, this));
    },

    onEdit: function (event) {
        API.gallery.get_image(this.props.image.id, $.proxy(function (data) {
            this.form.setValue(data);
            this.form.clearErrors();
            Popover.showElement(this.form.element);
        }, this));
        event.stopPropagation();
    },

    onDelete: function (event) {
        Popover.showReact(this.deleteDialog);
        event.stopPropagation();
    },

    onCopy: function () {
        event.stopPropagation();
    },

    onPreview: function () {
        Popover.showReact(React.createElement(ImagePreview, {imageSource: this.props.image.src}), "scale");
    },

    render: function () {
        var headerFlag = '';
        if (this.props.image.banner_candidate) {
            headerFlag = React.createElement("div", {className: "header-flag", title: "This image is a header candidate"}, "header")
        }
        return (
            React.createElement("div", {className: "item"}, 
                React.createElement("div", {className: "thumbnail", style: {'backgroundImage': 'url(' + this.props.image.src + ')'}, 
                     onClick: this.onPreview}, 
                    React.createElement("div", {className: "edit-wrap"}, React.createElement("i", {className: "fa fa-trash-o red", onClick: this.onDelete})), 
                    React.createElement("div", {className: "edit-wrap"}, React.createElement("i", {className: "fa fa-pencil blue", onClick: this.onEdit})), 
                    React.createElement("a", {href: this.props.image.src}, 
                        React.createElement("div", {className: "edit-wrap"}, React.createElement("i", {className: "fa fa-link blue", onClick: this.onCopy}))
                    ), 
                    React.createElement("div", {className: "info group"}, 
                        React.createElement("span", {className: "title"}, this.props.image.title), React.createElement("br", null), 
                        React.createElement("span", {className: "details"}, this.props.image.w, " x ", this.props.image.h, 
                            ", ", humanizeMemorySize(this.props.image.size)), 
                        headerFlag
                    )
                )
            ));
    }
});

},{"../../components/Dialog.jsx":4,"../../libs/API.js":10,"../../libs/Forms.js":11,"../../libs/Popover.js":13,"../../libs/std.js":14}],7:[function(require,module,exports){
require("../../libs/API.js");
require("../../libs/Matcher.js");


ImageListSearch = {};

ImageList = React.createClass({displayName: "ImageList",
    getInitialState: function () {
        return {data: [], matcher: EmptyMatcher};
    },

    fetchData: function () {
        var that = this;
        API.gallery.all(function (response) {
            that.setState({images: response});
        });
    },

    componentDidMount: function () {
        ImageListSearch.search = $.proxy(function (query) {
            this.state.matcher = new Matcher(query);
        }, this);

        ImageListSearch.clearSearch = $.proxy(function (query) {
            this.state.matcher = EmptyMatcher;
        }, this);

        this.fetchData();
        setInterval(this.fetchData, this.props.pollInterval);
    },

    componentWillUnmount: function () {
        clearInterval(this.fetchData);

        ImageListSearch.search = undefined;
        ImageListSearch.clearSearch = undefined;
    },

    hasImages: function () {
        return this.state.images !== undefined;
    },

    render: function () {
        var searchFilter = '';
        var hasFilter = false;
        if (this.state.matcher !== EmptyMatcher) {
            searchFilter =
                React.createElement("div", {className: "filter"}, "Only showing images with name: ", React.createElement("span", {
                    className: "value"}, this.state.matcher.query), 
                    React.createElement("i", {className: "fa fa-close", onClick: ImageListSearch.clearSearch})
                );
        }

        var imageNodes = "";
        if (this.hasImages()) {
            imageNodes = this.state.images.map($.proxy(function (image) {
                if (this.state.matcher.matches(image.title)) {
                    return (React.createElement(Image, {image: image, key: image.src}));
                } else {
                    return '';
                }

            }, this));
        }

        return (
            React.createElement("div", {className: "full-width gallery-admin group"}, 
                searchFilter, " ", React.createElement("br", null), 
                imageNodes
            )
        );
    }
});
},{"../../libs/API.js":10,"../../libs/Matcher.js":12}],8:[function(require,module,exports){
require("../../libs/Popover.js");

ImagePreview = React.createClass({displayName: "ImagePreview",
    onClick: function () {
        Popover.hide();
    },

    render: function () {
        return (
            React.createElement("div", {className: "image-preview"}, 
                React.createElement("img", {src: this.props.imageSource, className: "image", onClick: this.onClick})
            )
        );
    }
});
},{"../../libs/Popover.js":13}],9:[function(require,module,exports){
require("../components/Image.jsx");
require("../components/ImageList.jsx");
require("../components/ImagePreview.jsx");

require("../../admin/components/ActionBar.jsx");
require("../../admin/components/SearchBar.jsx");
require("../../admin/components/Menu.jsx");

require("../../components/Upload.jsx");

ReactDOM.render(React.createElement(ImageList, {pollInterval: 2000}), $(".content")[0]);

var galleryActions = [
    {
        name: "Upload Image",
        icon: "fa-plus",
        onClick: function () {
            Popover.showReact(React.createElement(Upload));
        }
    }
];

ReactDOM.render(React.createElement(ActionBar, {actions: galleryActions}), $('.buttons-wrap')[0]);

ReactDOM.render(React.createElement(Menu, {active: "Gallery"}), $(".content-wrap .menu")[0]);

ReactDOM.render(React.createElement(SearchBar, {onSearch: ImageListSearch.search}), $('.search-wrap')[0]);

},{"../../admin/components/ActionBar.jsx":1,"../../admin/components/Menu.jsx":2,"../../admin/components/SearchBar.jsx":3,"../../components/Upload.jsx":5,"../components/Image.jsx":6,"../components/ImageList.jsx":7,"../components/ImagePreview.jsx":8}],10:[function(require,module,exports){
//noinspection JSUnusedAssignmentre
API = {};

var ImageFormDescriptor = {
    'name': 'Image',
    'fields': [
        {
            'title': "Id",
            'name': "id",
            'type': "Hidden",
            'description': "The id of the image"
        },
        {
            'title': 'Title',
            'name': 'title',
            'type': 'Text',
            'description': 'The title of the image'
        },
        {
            'title': 'Description',
            'name': 'description',
            'type': 'Text',
            'description': 'Short description'
        },
        {
            'title': 'Banner Candidate',
            'name': 'bannerCandidate',
            'type': 'Boolean',
            'description': 'Can be used as a banner on the main page'
        },
        {
            'title': 'Gallery Item',
            'name': 'galleryItem',
            'type': 'Boolean',
            'description': 'Is visible on the gallery page'
        },
        {
            'title': 'Image Type',
            'name': 'imageType',
            'type': 'Choice',
            'choices': ['Sketch', 'Artwork', 'Screenshot'],
            'description': 'The category of the image'
        }
    ]
};

//noinspection JSUnusedGlobalSymbols
API.blog = {
    all: function (callback) {
        console.trace("[API] blog.all()");

        var url = "/api/articles/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    get_article: function (id, callback) {
        console.trace("[API] blog.get_article(" + id + ")");

        var url = "/api/articles/get/" + id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    add_tags: function (article_id, tags, callback) {
        console.trace("[API] blog.add_tags(" + article_id + ", " + tags + ")");

        var url = "/blog/tags/add/" + article_id + "/" + tags;
        $.post(url, function (data) {
            callback(data);
        });
    },

    remove_tag: function (article_id, tag, callback) {
        console.trace("[API] blog.remove_tag(" + article_id + ", " + tags + ")");

        tag = $.trim(tag);
        var url = "/blog/tags/remove/" + article_id + "/" + tag;
        $.post(url, function (data) {
            callback(data);
        });
    },

    get_tags: function (article_id, callback) {
        console.trace("[API] blog.get_tags(" + article_id + ")");

        var url = "/blog/tags/get/" + article_id;
        $.post(url, function (data) {
            callback(data);
        });
    },

    markdown: function (text, callback) {
        //console.trace("[API] blog.markdown("+text+")");

        var url = "/api/articles/markdown/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: text,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'text/plain; charset=UTF-8'
        });
    },

    save_article: function (article, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article);

        var url = "/api/articles/save/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify(article),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_article: function (article_id, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article_id);

        var url = "/api/articles/delete/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    publish_article: function (article_id, callback) {
        console.trace("[API] blog.publish_article()");
        console.trace(article_id);

        var url = "/api/articles/publish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    unpublish_article: function (article_id, callback) {
        console.trace("[API] blog.unpublish_article()");
        console.trace(article_id);

        var url = "/api/articles/unpublish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};

API.gallery = {
    all: function (callback) {
        var url = "/api/gallery/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    image_descriptor: function (callback) {
        callback(ImageFormDescriptor);
    },

    get_image: function (image_id, callback) {
        var url = "/api/gallery/image/get/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    update_image: function (image, callback) {
        image['json'] = true;

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: "/api/gallery/image/update/" + image['id'],
            data: JSON.stringify(image),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_image: function (image_id, callback) {
        var url = "/api/gallery/image/delete/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    upload_image: function (file, onSuccess, onProgress) {
        var url = "/api/gallery/upload";
        var file_data = new FormData();
        file_data.append("file", file);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = event.loaded / event.total;
                        percentComplete = parseInt(percentComplete * 100);
                        onProgress(percentComplete);
                    }
                }, false);

                return xhr;
            },
            url: url,
            type: "POST",
            data: file_data,
            processData: false,
            contentType: false,
            success: function (response) {
                onSuccess(response);
            }
        });
    }
};

API.auth = {
    login: function (username, password, callback) {
        console.trace("[API] auth.login(username, password)");
        console.trace(username);

        var url = "/api/auth/login";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'username': username, 'password': password}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    logged_user: function (callback) {
        console.trace("[API] auth.logged_user()");

        var url = "/api/auth/logged_user";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};
},{}],11:[function(require,module,exports){
uuid4 = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        //noinspection JSUnresolvedFunction,JSUnresolvedVariable
        var r = crypto.getRandomValues(new Uint8Array(1))[0] % 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
};

isEnterKey = function (key) {
    return key == 13;
};

Form = function (modelDescriptor) {

    this.init = function () {
        this.id = uuid4();

        this.fields = [];

        this.descriptor = modelDescriptor;

        this.applyButton = $('<a>').addClass("button").addClass("blue").html("Save");
        this.cancelButton = $("<a>").addClass("button").addClass("red").html("Cancel");

        modelDescriptor['fields'].forEach($.proxy(function (fieldDescriptor) {
            var field = this.createField(fieldDescriptor);
            this.appendField(field);
        }, this));

        this.render();
    };

    this.setTitle = function (title) {
        this.header.html(title);
    };

    this.createField = function (fieldDescriptor) {
        switch (fieldDescriptor.type) {
            case "Text":
                var textField = new TextField(fieldDescriptor);
                textField.editor.keypress($.proxy(function (event) {
                    if (isEnterKey(event.which)) {
                        this.applyButton.click();
                        event.preventDefault();
                    }
                }, this));
                return textField;
            case "Boolean":
                return new BooleanField(fieldDescriptor);
            case "Choice":
                return new ChoiceField(fieldDescriptor);
            case "Hidden":
                return new HiddenField(fieldDescriptor);
        }
    };

    this.appendField = function (field) {
        this.fields.push(field);
    };

    this.render = function () {
        this.element = $("<div>")
            .attr("id", this.id)
            .addClass("form")
            .addClass("dialog");

        this.header = $("<h1>")
            .html(this.descriptor['title']);

        this.errors = $("<div>").addClass("errors").hide();

        var buttonWrap = $("<div>")
            .addClass("buttons")
            .append(this.applyButton)
            .append(this.cancelButton);

        this.element.append(this.header);

        this.element.append(this.errors);

        this.fields.forEach($.proxy(function (field) {
            this.element.append(field.element);
        }, this));

        this.element.append(buttonWrap);

        return this.element;
    };

    this.setValue = function (object) {
        this.fields.forEach(function (field) {
            field.setValue(object[field.descriptor['name']]);
        });
    };

    this.getValue = function () {
        var object = {};
        this.fields.forEach(function (field) {
            object[field.descriptor['name']] = field.getValue();
        });
        return object;
    };

    this.onApply = function (callback) {
        this.applyButton.click(callback);
    };

    this.onCancel = function (callback) {
        this.cancelButton.click(callback);
    };

    this.setErrors = function (errors) {
        var errorList = $("<ul>");
        errors.forEach(function (error) {
            var errorElement = $("<li>").html(error);
            errorList.append(errorElement);
        });
        this.errors.append(errorList);
        this.errors.slideDown();
    };

    this.clearErrors = function () {
        this.errors.empty();
        this.errors.hide();
    };

    this.init();
};

/**
 * Field used to edit lines of text
 */
function TextField(fieldDescriptor) {

    this.init = function () {
        this.descriptor = fieldDescriptor;

        this.elementClass = "_field__" + this.descriptor['name'];

        this.element = $("<div>");

        this.editor = $("<input>")
            .addClass("text-field")
            .addClass(this.elementClass);

        var label = $("<div>")
            .addClass("label")
            .html(this.descriptor['title']);

        this.element.append(this.editor);
        this.element.append(label);
    };

    this.getValue = function () {
        return this.editor.val();
    };

    this.setValue = function (value) {
        this.editor.val(value);
    };

    this.init();
}

/**
 * Field used to edit boolean values
 */
function BooleanField(fieldDescriptor) {
    this.init = function () {
        this.descriptor = fieldDescriptor;

        this.elementClass = "_field__" + this.descriptor['name'].replace(" ", "");

        this.element = $("<div>");
        this.element.addClass("boolean-field");
        this.element.addClass(this.elementClass);

        var label = $('<div class="label">' + this.descriptor['title'] + '</div>');

        this.editor = $("<div>");
        this.editor.addClass("editor");
        this.editor.click($.proxy(function () {
            this.toggleValue();
        }, this));

        this.checkMarkWrap = $('<div class="check-mark-wrap"></div>');
        this.checkMark = $('<i class="fa fa-check check-mark off"></i>');

        this.checkMarkWrap.append(this.checkMark);

        var details = $('<span class="details">' + this.descriptor['description'] + '</span>');

        this.editor.append(this.checkMarkWrap);
        this.editor.append(details);

        this.element.append(label);
        this.element.append(this.editor);
    };

    this.getValue = function () {
        return this.checkMark.hasClass('on');
    };

    this.setValue = function (value) {
        if (value != this.getValue()) {
            this.checkMark.removeClass("on").removeClass("off").addClass(value ? "on" : "off");
        }
        this.element.value = value;
    };

    this.toggleValue = function () {
        this.setValue(!this.getValue());
    };

    this.init();
}

/**
 * Field used to edit enums
 */
function ChoiceField(fieldDescriptor) {
    this.init = function () {
        this.descriptor = fieldDescriptor;
        this.elementClass = "_field__" + this.descriptor['name'];


        this.element = $("<div>");
        this.element.addClass("select-field");
        this.element.addClass(this.elementClass);

        var title = $("<div>");
        title.addClass("title");
        title.html(this.descriptor['title']);
        this.element.append(title);

        var icon = $("<i>");
        icon.addClass("fa").addClass("fa-chevron-down");
        this.element.append(icon);

        this.editor = $("<select>");
        this.editor.addClass("editor");
        this.element.append(this.editor);

        this.descriptor['choices'].forEach($.proxy(function (item) {
            var option = $("<option>");
            option.attr("value", item);
            option.html(item);
            this.editor.append(option);
        }, this));
    };

    this.getValue = function () {
        return this.editor.value;
    };

    this.setValue = function (value) {
        if (this.descriptor['choices'].indexOf(value) >= 0) {
            this.editor.value = value;
        }
    };

    this.init();
}

function HiddenField(descriptor) {
    this.descriptor = descriptor;

    this.element = '';

    this.value = '';

    this.setValue = function (value) {
        this.value = value;
    };

    this.getValue = function () {
        return this.value;
    };

    this.render = function () {
        return "";
    }
}

},{}],12:[function(require,module,exports){
/**
 * Creates a Matcher object used to apply searcth queries over items or arrays of items.
 *
 * @param query a user search query
 */
Matcher = function (query) {

    // the search query is split into lowercase words
    this.words = query.split(' ').map(function (word) {
        return word.toLowerCase();
    });

    this.query = query;

    /**
     * Tests a single value against a matcher
     * @param matcher the matcher used to test the value against
     * @param value the requested value
     * @returns {*} true if the matcher matches the value. false otherwise
     */
    function matchesSingleValue(matcher, value) {
        if (value.constructor != String) {
            return false;
        }

        var partialMatches = matcher.words.map(function (word) {
            return value.toLocaleLowerCase().includes(word);
        });

        return partialMatches.reduce(function (acc, current) {
            return acc && current;
        }, true);
    }

    /**
     * Tests a single value or an array of values against a matcher
     * @param value either one String or an Array of Strings to be matched by the matcher
     * @returns {*} either true or false if a single value was matched by the string, or an array of match results
     */

    this.matches = function (value) {
        var thisMatcher = this;
        if (value.constructor == String) {
            return matchesSingleValue(thisMatcher, value);
        }

        if (value.constructor == Array) {
            return value.map(function (item) {
                return matchesSingleValue(thisMatcher, item);
            });
        }

        return false;
    }
};

/**
 * Empty matcher that matches ANYTHING
 * @type {Matcher}
 */
EmptyMatcher = new Matcher('');

},{}],13:[function(require,module,exports){
Popover = new (function () {
    var CONTAINER_ID = "ktzPopoverContainer";

    var ELEMENT_REACT = 0;
    var ELEMENT_DOM = 1;
    var ELEMENT_NONE = 2;

    var install = $.proxy(function () {
        if (isInstalled()) {
            return;
        }

        var body = $("body");
        body.append(this.cover);
        body.keydown(function (event) {
            if (event.which == 27) {
                Popover.hide();
            }
        });

        console.log("Popover installed");
    }, this);

    var isInstalled = function () {
        return $("#" + CONTAINER_ID).length == 1;
    };

    var removeAnimationClasses = $.proxy(function () {
        var container = this.container;
        container.classList().forEach(function (element) {
            if (element.startsWith("animation-")) {
                container.removeClass(element);
            }
        });
    }, this);

    var animateShow = $.proxy(function (animationClass) {
        animationClass = animationClass || "default";
        removeAnimationClasses();
        this.container.addClass("animation-show-" + animationClass);

        this.cover.css("visibility", "visible");
        this.cover.css("opacity", "0");
        this.cover.animate({
            opacity: 1.0
        }, 100);
    }, this);

    var animateHide = $.proxy(function (animation) {
        removeAnimationClasses();
        animation = animation || "default";
        this.container.addClass("animation-hide-" + animation);

        // animate opacity to 0 then set visibility to hidden
        this.cover.animate({
                opacity: .0
            }, 100, $.proxy(function () {
                this.cover.css("visibility", "hidden");
            }, this)
        );
    }, this);

    this.showElement = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountDomElement(element);
        animateShow(animation);
    };

    this.showReact = function (element, animation) {
        this.cover.css("opacity", "1");
        this.animation = animation;

        unmountClient();
        mountReact(element);
        animateShow(animation);
    };

    this.hide = function () {
        animateHide(this.animation);
    };

    var mountReact = $.proxy(function (element) {
        ReactDOM.render(element, this.container[0]);
        this.elementType = ELEMENT_REACT;
    }, this);

    var unmountReact = $.proxy(function () {
        ReactDOM.unmountComponentAtNode(this.container[0]);
        this.elementType = ELEMENT_NONE;
    }, this);

    var mountDomElement = $.proxy(function (element) {
        this.clientElement = element;
        this.container.append(this.clientElement);
        this.elementType = ELEMENT_DOM;
    }, this);

    var unmountDomElement = $.proxy(function () {
        if (this.clientElement !== undefined) {
            this.clientElement.detach();
        }
        this.elementType = ELEMENT_NONE;
    }, this);

    var unmountClient = $.proxy(function () {
        if (this.elementType == ELEMENT_DOM) {
            unmountDomElement();
        }
        if (this.elementType == ELEMENT_REACT) {
            unmountReact();
        }
    }, this);

    // Constructor
    /**
     * Container for the actual popover contents
     */
    this.container = $("<div></div>");
    this.container.addClass("content");

    /**
     * The cover that hides the underlying page
     */
    this.cover = $("<div>");
    this.cover.addClass("modal-popup");
    this.cover.attr("id", CONTAINER_ID);
    this.cover.css("opacity", "0");

    this.cover.append(this.container);

    install();
})();
},{}],14:[function(require,module,exports){
/**
 * Created by Alex on 30/11/2015.
 */

humanizeMemorySize = function humanizeMemorySize(bytes) {
    bytes = parseInt(bytes);
    var unitMultiples = ["b", "Kb", "Mb", "Gb", "Tb"];
    var unit = 0;
    while (bytes > 1000) {
        unit += 1;
        bytes /= 1000;
    }
    return parseInt(bytes * 100) / 100 + unitMultiples[unit];
};
},{}]},{},[9]);
