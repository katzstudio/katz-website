(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
require("../../libs/API.js");

LoginForm = React.createClass({displayName: "LoginForm",
    getInitialState: function () {
        return {'loading': false};
    },

    componentDidMount: function () {
        var username = $(this.refs.username);
        var password = $(this.refs.password);

        var returnKeyHandler = $.proxy(function (event) {
            if (event.which == 13) {
                this.onLogin();
                event.stopPropagation();
            }
        }, this);

        username.on("keypress", returnKeyHandler);

        password.on("keypress", returnKeyHandler);
    },

    onLogin: function () {
        this.setState({'loading': true});
        var username = $(this.refs.username).val();
        var password = $(this.refs.password).val();
        API.auth.login(username, password, $.proxy(function (responseData, responseStatus) {
            if (responseStatus['success']) {
                window.location = "/admin/";
            } else {
                this.setState({'loading': false});
                var error = $(this.refs.error);
                error.html(responseStatus['reason']);
                error.slideDown()
            }
        }, this));
    },

    render: function () {
        var buttonContent = '';
        if (this.state.loading) {
            buttonContent = React.createElement("i", {className: "fa fa-refresh fa-spin"})
        } else {
            buttonContent = "Log In";
        }
        return (
            React.createElement("div", {className: "overlay"}, 
                React.createElement("div", {className: "login-form-wrap"}, 
                    React.createElement("div", {className: "login-form"}, 
                        React.createElement("span", {className: "title"}, "Log In"), 
                        React.createElement("div", {className: "error", ref: "error"}), 
                        React.createElement("i", {className: "fa fa-user"}), 
                        React.createElement("input", {ref: "username"}), 
                        React.createElement("i", {className: "fa fa-lock"}), 
                        React.createElement("input", {ref: "password", type: "password"}), 
                        React.createElement("div", {className: "button", onClick: this.onLogin}, buttonContent)
                    )
                )
            )
        );
    }
});

},{"../../libs/API.js":3}],2:[function(require,module,exports){
require('../components/LoginForm.jsx');

ReactDOM.render(React.createElement(LoginForm, null), $(".inner-body #content")[0]);

},{"../components/LoginForm.jsx":1}],3:[function(require,module,exports){
//noinspection JSUnusedAssignmentre
API = {};

var ImageFormDescriptor = {
    'name': 'Image',
    'fields': [
        {
            'title': "Id",
            'name': "id",
            'type': "Hidden",
            'description': "The id of the image"
        },
        {
            'title': 'Title',
            'name': 'title',
            'type': 'Text',
            'description': 'The title of the image'
        },
        {
            'title': 'Description',
            'name': 'description',
            'type': 'Text',
            'description': 'Short description'
        },
        {
            'title': 'Banner Candidate',
            'name': 'bannerCandidate',
            'type': 'Boolean',
            'description': 'Can be used as a banner on the main page'
        },
        {
            'title': 'Gallery Item',
            'name': 'galleryItem',
            'type': 'Boolean',
            'description': 'Is visible on the gallery page'
        },
        {
            'title': 'Image Type',
            'name': 'imageType',
            'type': 'Choice',
            'choices': ['Sketch', 'Artwork', 'Screenshot'],
            'description': 'The category of the image'
        }
    ]
};

//noinspection JSUnusedGlobalSymbols
API.blog = {
    all: function (callback) {
        console.trace("[API] blog.all()");

        var url = "/api/articles/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    get_article: function (id, callback) {
        console.trace("[API] blog.get_article(" + id + ")");

        var url = "/api/articles/get/" + id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    add_tags: function (article_id, tags, callback) {
        console.trace("[API] blog.add_tags(" + article_id + ", " + tags + ")");

        var url = "/blog/tags/add/" + article_id + "/" + tags;
        $.post(url, function (data) {
            callback(data);
        });
    },

    remove_tag: function (article_id, tag, callback) {
        console.trace("[API] blog.remove_tag(" + article_id + ", " + tags + ")");

        tag = $.trim(tag);
        var url = "/blog/tags/remove/" + article_id + "/" + tag;
        $.post(url, function (data) {
            callback(data);
        });
    },

    get_tags: function (article_id, callback) {
        console.trace("[API] blog.get_tags(" + article_id + ")");

        var url = "/blog/tags/get/" + article_id;
        $.post(url, function (data) {
            callback(data);
        });
    },

    markdown: function (text, callback) {
        //console.trace("[API] blog.markdown("+text+")");

        var url = "/api/articles/markdown/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: text,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'text/plain; charset=UTF-8'
        });
    },

    save_article: function (article, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article);

        var url = "/api/articles/save/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify(article),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_article: function (article_id, callback) {
        console.trace("[API] blog.save_article()");
        console.trace(article_id);

        var url = "/api/articles/delete/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    publish_article: function (article_id, callback) {
        console.trace("[API] blog.publish_article()");
        console.trace(article_id);

        var url = "/api/articles/publish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    unpublish_article: function (article_id, callback) {
        console.trace("[API] blog.unpublish_article()");
        console.trace(article_id);

        var url = "/api/articles/unpublish/";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'id': article_id}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};

API.gallery = {
    all: function (callback) {
        var url = "/api/gallery/all";
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    image_descriptor: function (callback) {
        callback(ImageFormDescriptor);
    },

    get_image: function (image_id, callback) {
        var url = "/api/gallery/image/get/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    update_image: function (image, callback) {
        image['json'] = true;

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: "/api/gallery/image/update/" + image['id'],
            data: JSON.stringify(image),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    delete_image: function (image_id, callback) {
        var url = "/api/gallery/image/delete/" + image_id;
        $.post(url, function (response) {
            callback(response['data'], response['status']);
        });
    },

    upload_image: function (file, onSuccess, onProgress) {
        var url = "/api/gallery/upload";
        var file_data = new FormData();
        file_data.append("file", file);

        $.ajax({
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (event) {
                    if (event.lengthComputable) {
                        var percentComplete = event.loaded / event.total;
                        percentComplete = parseInt(percentComplete * 100);
                        onProgress(percentComplete);
                    }
                }, false);

                return xhr;
            },
            url: url,
            type: "POST",
            data: file_data,
            processData: false,
            contentType: false,
            success: function (response) {
                onSuccess(response);
            }
        });
    }
};

API.auth = {
    login: function (username, password, callback) {
        console.trace("[API] auth.login(username, password)");
        console.trace(username);

        var url = "/api/auth/login";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            data: JSON.stringify({'username': username, 'password': password}),
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    },

    logged_user: function (callback) {
        console.trace("[API] auth.logged_user()");

        var url = "/api/auth/logged_user";

        var callbackWrapper = function (response) {
            callback(response['data'], response['status']);
        };

        $.ajax({
            url: url,
            method: "POST",
            success: callbackWrapper,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8'
        });
    }
};
},{}]},{},[2]);
