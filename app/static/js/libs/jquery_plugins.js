(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/**
 * Sets the selection of the editor. If the end of the selection is not
 * specified, then the cursor is placed at the start of the selection.
 *
 * @method setSelection
 * @param {Number} from the start of the selection
 * @param {Number} to the end of the selection.
 */
new function ($) {
    $.fn.setSelection = function (from, to) {
        if (typeof from === 'undefined') {
            return;
        }

        to = (typeof to !== 'undefined' ? to : from);

        var element = this[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(from, to);
        } else if (element.createTextRange) {
            var range = element.createTextRange();
            range.collapse(true);
            range.moveStart('character', from);
            range.moveEnd('character', to);
            range.select();
        }
    }
}(jQuery);

/**
 * Sets the cursor position inside the editor.
 *
 * @method setCursor
 * @param {Number} cursorPosition the new position of the cursor.
 */
new function ($) {
    $.fn.setCursor = function (cursorPosition) {
        return $.fn.setSelection(cursorPosition);
    }
}(jQuery);

/**
 * Returns the selection of the editor.
 *
 * @method getSelection
 * @return {Array} an array containing the [start, end] of the selection
 */
new function ($) {
    $.fn.getSelection = function () {
        var element = this[0];
        return [element.selectionStart, element.selectionEnd]
    }
}(jQuery);

/**
 * Returns the current position of the cursor inside the editor
 *
 * @method getCursor
 * @return {Number} the position of the cursor
 */
new function ($) {
    $.fn.getCursor = function () {
        var element = this[0];
        return element.selectionEnd;
    }
}(jQuery);

/**
 * Returns a list of all the classes of an element
 *
 * @method classList
 * @return {Array} a list of classes associated with the element
 */
new function ($) {
    $.fn.classList = function () {
        return this[0].className.split(/\s+/);
    };
}(jQuery);

},{}]},{},[1]);
