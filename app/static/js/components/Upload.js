(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
UploadLoadingBar = React.createClass({displayName: "UploadLoadingBar",
    getInitialState: function () {
        return {
            filename: 'Forest_01.jpeg',
            percent: 0,
            file_size: "5Mb"
        }
    },

    onUploadAnother: function () {
        if (this.props.onUploadAnother !== undefined) {
            this.props.onUploadAnother();
        }
    },

    onClose: function () {
        Popover.hide();
    },

    render: function () {
        var percent = this.props.status.percent;
        var totalSize = this.props.status.file_size;
        var uploaded = (totalSize * percent) / 100;

        var filenameInfo;
        if (this.props.status.percent < 100) {
            filenameInfo = "Uploading " + this.props.status.filename;
        } else {
            filenameInfo = "Uploaded " + this.props.status.filename;
        }

        var progressInfo;
        if (this.props.status.percent < 100) {
            progressInfo = humanizeMemorySize(uploaded) + " / " + humanizeMemorySize(totalSize);
        } else {
            progressInfo = humanizeMemorySize(totalSize);
        }

        var loadingFillStyle = {
            width: this.props.status.percent + "%"
        };

        var buttonsStyle = {
            transition: "0.3s",
            height: percent == 100 ? "56px" : "0px",
            overflow: "hidden",
            paddingTop: "10px"
        };

        return (
            React.createElement("div", {className: "upload-component"}, 
                React.createElement("h1", null, "Upload"), 
                React.createElement("div", {className: "progress-bar"}, 
                    React.createElement("div", {className: "info-wrap"}, 
                        React.createElement("div", {className: "percent"},  percent, "%"), 
                        React.createElement("div", {className: "info"}, 
                            React.createElement("span", {className: "first-line"}, filenameInfo), 
                            React.createElement("span", {className: "second-line"}, progressInfo)
                        )

                    ), 
                    React.createElement("div", {className: "fill", style: loadingFillStyle}
                    )
                ), 
                React.createElement("div", {className: "buttons", style: buttonsStyle}, 
                    React.createElement("div", {className: "button blue", onClick: this.onUploadAnother}, "Another One"), 
                    React.createElement("div", {className: "button red", onClick: this.onClose}, "Close")
                )
            )
        );
    }
});

UploadDropzone = React.createClass({displayName: "UploadDropzone",
    waitingMessage: "Drop image here to upload",

    hoveredMessage: "Drop it like it's hot!",

    getInitialState: function () {
        return {hovered: false};
    },

    componentDidMount: function () {
        var dropzoneAndChildren = $(".dropzone *");

        dropzoneAndChildren.bind('dragenter', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: true});
        }, this));

        dropzoneAndChildren.bind('dragover', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();
        }, this));

        dropzoneAndChildren.bind('dragleave', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: false});
        }, this));

        dropzoneAndChildren.bind('drop', $.proxy(function (event) {
            event.preventDefault();
            event.stopPropagation();

            this.setState({hovered: false});
            this.props.onDrop(event);
        }, this))
    },

    render: function () {
        var dropzoneClass = 'dropzone ' + (this.state.hovered ? 'active' : '');
        var message = this.state.hovered ? this.hoveredMessage : this.waitingMessage;
        return (
            React.createElement("div", {className: "upload-component waiting"}, 
                React.createElement("div", {className: dropzoneClass}, 
                    React.createElement("span", {className: "message"}, message)
                )
            )
        );
    }
});

Upload = React.createClass({displayName: "Upload",
    getInitialState: function () {
        return {
            uploading: false
        };
    },

    onUploadSuccess: function (response) {
        console.log("onUploadSuccess " + response);
    },

    onUploadProgress: function (progress) {
        console.log("onUploadProgress " + progress);
        this.setState({progress: progress});
    },

    onDrop: function (event) {
        var file = event.originalEvent.dataTransfer.files[0];
        API.gallery.upload_image(file, this.onUploadSuccess, this.onUploadProgress);
        this.setState({uploading: true, progress: 0, file: file});
    },

    onUploadAnother: function () {
        this.setState({uploading: false});
    },

    render: function () {
        if (this.state.uploading) {
            var uploadStatus = {
                filename: this.state.file.name,
                percent: this.state.progress,
                file_size: this.state.file.size
            };
            return (React.createElement(UploadLoadingBar, {status: uploadStatus, onUploadAnother: this.onUploadAnother}));
        } else {
            return (React.createElement(UploadDropzone, {onDrop: this.onDrop}));
        }
    }
})
;
},{}]},{},[1]);
