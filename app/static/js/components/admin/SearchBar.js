(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
SearchBar = React.createClass({displayName: "SearchBar",
    onSearch: function (value) {
        if (this.props.onSearch !== undefined) {
            this.props.onSearch(value);
        }
    },

    onKeyDown: function (event) {
        if (event.which == 13) {
            var input = $(this.refs.input);
            var search = $(this.refs.search);
            if (input.val() != "") {
                this.props.onSearch(input.val());
                input.val("");
            }
            search.removeClass("extended");
            $(input).blur();
        }
    },

    onClick: function () {
        var search = $(this.refs.search);
        var input = $(this.refs.input);
        if (!search.hasClass("extended")) {
            search.addClass("extended");
            input.focus();
        }
    },

    onBlur: function () {
        $(this.refs.search).removeClass("extended");
    },

    render: function () {
        return (
            React.createElement("div", {className: "search", ref: "search", onClick: this.onClick}, 
                React.createElement("i", {className: "fa fa-search"}), 
                React.createElement("input", {ref: "input", spellCheck: "false", onKeyDown: this.onKeyDown, onBlur: this.onBlur})
            )
        );
    }
});

},{}]},{},[1]);
