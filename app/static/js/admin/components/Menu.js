(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
Menu = React.createClass({displayName: "Menu",
    getDefaultProps: function () {
        return {
            items: [
                {name: "Dashboard", icon: "fa-dashboard", url: "#", disabled: true},
                {name: "Blog", icon: "fa-pencil", url: "/admin/articles"},
                {name: "Gallery", icon: "fa-camera", url: "/admin/gallery"},
                {name: "Users", icon: "fa-user", url: "#", disabled: true},
                {name: "Settings", icon: "fa-cog", url: "#", disabled: true},
                {name: "Messages", icon: "fa-envelope", url: "#", disabled: true}
            ]
        };
    },

    render: function () {
        var activeItem = this.props.active;
        var items = this.props.items.map(function (item) {
            var iconClass = "fa " + item.icon;
            var wrapperClass = "item";
            if (item.name === activeItem) {
                wrapperClass += " active";
            }
            if (item.disabled) {
                wrapperClass += " disabled";
            }

            return (React.createElement("a", {href: item.url, key: item.name}, React.createElement("div", {className: wrapperClass}, 
                React.createElement("i", {className: iconClass})
            )));
        });
        return (React.createElement("div", null, items));
    }
});

},{}]},{},[1]);
