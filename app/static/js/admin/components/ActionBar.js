(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
Action = React.createClass({displayName: "Action",
    getInitialState: function () {
        return {
            'icon': this.props.action.icon,
            'text': this.props.action.text,
            'class': this.props.action.class !== undefined ? this.props.action.class : '',
            'href': this.props.action.href
        }
    },

    setText: function (text) {
        this.setState({text: text});
    },

    setIcon: function (icon) {
        this.setState({icon: icon});
    },

    setClass: function (buttonClass) {
        this.setState({class: buttonClass});
    },

    onClick: function (event) {
        this.props.action.onClick(this);
    },

    componentDidMount: function () {
        if (this.props.action.onInit !== undefined) {
            this.props.action.onInit(this);
        }
    },

    render: function () {
        var iconClass = "fa " + this.state.icon;
        var textComponent = undefined;
        if (this.props.action.text !== undefined && this.state.text != "") {
            textComponent = React.createElement("span", {ref: "text"}, this.state.text);
        }

        var buttonClass = "button " + this.state.class;

        var button = (
            React.createElement("div", {className: buttonClass, onClick: this.onClick}, 
                React.createElement("i", {className: iconClass, ref: "icon"}), 
                textComponent
            ));

        if (this.props.action.href !== undefined) {
            return React.createElement("a", {href: this.props.action.href}, button)
        } else {
            return button;
        }
    }
});

ActionBar = React.createClass({displayName: "ActionBar",
    render: function () {
        var buttons = this.props.actions.map(function (action, index) {
            return (React.createElement(Action, {action: action, key: index}));
        });

        return (
            React.createElement("div", {className: "buttons"}, 
                buttons
            )
        );
    }
});

},{}]},{},[1]);
