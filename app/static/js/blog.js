var TITLE_SHADOW = "Enter a title";

function substr(e, from, to) {
    return e.val().substring(from, to);
}

function insert(e, text, position) {
    if (position == undefined) {
        position = e.getCursor();
    }
    e.val(substr(e, 0, position) + text + substr(e, position));
}

function decorate(e, decoration) {
    var selection = e.getSelection();
    if (selection[0] == selection[1]) {
        return;
    }

    e.val(substr(e, 0, selection[0]) +
        decoration +
        substr(e, selection[0], selection[1]) +
        decoration +
        substr(e, selection[1]));
}

function updateArticle() {
    var url = "/blog/update/" + article_id();

    // Gather article data
    var title_value = $("#title").val();
    if (title_value == TITLE_SHADOW) {
        title_value = "";
    }

    var content_value = $(".editor").val();

    var data = {"title": title_value, "content": encodeURIComponent(content_value)};

    // request to update the article
    $.post(url, $.param(data, true));
}

$(document).ready(function () {
    var e = $(".editor");

    $(".bold").click(function () {
        decorate(e, "**");
    });

    $(".italic").click(function () {
        decorate(e, "*");
    });

    $(".header").click(function () {
        var previousLineBreak = e.val().substring(0, e.getCursor()).lastIndexOf("\n");
        var insertPosition = 0;
        if (previousLineBreak != -1) {
            insertPosition = previousLineBreak + 1;
        }

        insert(e, "## ", insertPosition);
    });

    $(".list").click(function () {
        insert(e, "\n\n * Item 0\n\n");
    });

    $(".link").click(function () {
        insert(e, "[example link](http://example.com/)");
    });

    $(".image").click(function () {
        insert(e, "![alt text](/path/to/img.jpg \"Title\")");
    });

    $(".editor-widget.widget-tags .button").click(function () {
        var inputText = $(".editor-widget.widget-tags input");
        var tags = inputText.val();
        inputText.val("");
        API.blog.add_tags(article_id(), tags, render_tags_callback);
    });

    $(".editor-widget.widget-tags input").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(".editor-widget.widget-tags .button").click();
        }
    });

    setInterval(updateArticle, 3000);

    $(document).keydown(function (event) {
        if (!( String.fromCharCode(event.which).toLowerCase() == 's' && event.ctrlKey) && !(event.which == 19)) return true;

        updateArticle();
        console.log("Updated the article");
        event.preventDefault();
        return false;
    });

    var title = $('#title');
    var title_editor = title[0];

    if (title_editor.value == '') {
        title_editor.value = TITLE_SHADOW;
    }

    title.attr('autocomplete', 'off');

    title.focus(function () {
        if (title_editor.value == TITLE_SHADOW) {
            title_editor.value = "";
        }
    });

    title.blur(function () {
        if (title_editor.value == '') {
            title_editor.value = TITLE_SHADOW;
        }
    });

    attach_tag_listeners();

    initialize();
});

function initialize() {
    API.blog.get_tags(article_id(), render_tags_callback);
}

function article_id() {
    return $("#id").val();
}

function render_tags_callback(data) {
    render_tags(data['tags']);
}

/**
 * Adds onclick handlers to every tag's delete button
 */
function attach_tag_listeners() {
    $(".editor-widget .tag a").click(function (event) {
        var tag_name = event.target.parentElement.parentElement.innerText;
        API.blog.remove_tag(article_id(), tag_name, render_tags_callback);
    });
}


/**
 * Adds HTML components for every tag in the tags list
 *
 * @param tags the list of tags to render
 */
function render_tags(tags) {
    var tags_container = $("#tag-list");
    tags_container.empty();

    tags.forEach(function (element) {
        tags_container.append(create_tag_element(element));
    });

    attach_tag_listeners();
}

/**
 * Returns a HTML representation of a tag
 *
 * @param tag_name the name of the tag
 * @returns {string} a HTML representation of the tag
 */
function create_tag_element(tag_name) {
    return "<div class=\"tag\">" + tag_name + " <a href=\"#\"><i class=\"fa fa-close\"></i></a></div>";
}

$(document).unload(function () {
    updateArticle();
});
