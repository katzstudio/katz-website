from datetime import datetime
from urllib.parse import urljoin
from uuid import uuid4

import flask
from flask import render_template, url_for, request
from flask_login import login_required
from flask_misaka import markdown
from sqlalchemy import desc
from werkzeug.contrib.atom import AtomFeed

from app.admin import admin
from app.blog import blog
from app.blog.models import Article


@admin.route("/articles/")
@login_required
def blog_admin():
    page = {
        'js': 'blog/views/article_list.js',
        'title': 'Articles',
        'subtitle': 'The story so far'
    }
    articles = Article.query.filter(Article.is_deleted == False)

    return render_template("admin.html", page=page, articles=articles)


# noinspection PyUnusedLocal
@admin.route("/articles/write/<article_id>/", methods=['GET', 'POST'])
@login_required
def article_write(article_id):
    page = {
        'js': 'blog/views/write_article.js',
        'title': 'Write Article',
        'subtitle': 'Have a coffee. Write a story'
    }

    return render_template("admin.html", page=page)


@admin.route("/articles/write/", methods=['GET'])
@login_required
def article_write_noid():
    return flask.redirect("/admin/articles/write/" + str(uuid4()), code=302)


@blog.route("/<article_public_url>")
def show(article_public_url):
    article = Article.query.filter(Article.public_url == article_public_url).first_or_404()

    sorted_articles = Article.query.filter(Article.is_published == True).order_by(Article.created_timestamp).all()
    article_index = sorted_articles.index(article)
    next_article = sorted_articles[article_index + 1] if article_index < len(sorted_articles) - 1 else None
    previous_article = sorted_articles[article_index - 1] if article_index > 0 else None

    markdown(article.content)

    return render_template(
            "blog/show.html",
            article=article,
            next_article=next_article,
            previous_article=previous_article,
            headlines=get_headline_articles())


@blog.route("/feed")
def rss_feed():
    def make_external(url):
        return urljoin(request.url_root, url)

    def to_datetime(date):
        return datetime.combine(date, datetime.min.time())

    feed = AtomFeed("Recent Articles", feed_url=request.url, url=request.url_root)
    articles = get_headline_articles()
    for article in articles:
        feed.add(
                article.title,
                markdown(article.content).unescape(),
                content_type="html",
                author=article.author,
                url=make_external(url_for('blog.show', article_public_url=article.public_url)),
                updated=to_datetime(article.modified_timestamp),
                published=to_datetime(article.created_timestamp))
    return feed.get_response()


def get_headline_articles():
    return Article.query \
        .filter((Article.is_published == True)) \
        .order_by(desc(Article.created_timestamp)) \
        .limit(5) \
        .all()
