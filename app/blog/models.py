from app import db

timestamp = db.func.current_timestamp

article_tags = db.Table('article_tags',
                        db.Column('tag_name', db.String(32), db.ForeignKey('tags.name'), primary_key=True),
                        db.Column('article_id', db.String(36), db.ForeignKey('articles.id'), primary_key=True))


class Article(db.Model):
    __tablename__ = "articles"

    id = db.Column(db.String(36), primary_key=True)
    title = db.Column(db.String(128), index=True)
    public_url = db.Column(db.String(128), index=True)

    author_username = db.Column(db.String(32), db.ForeignKey('users.username'), nullable=False)
    author = db.relationship('User')

    intro_paragraph = db.Column(db.Text)
    content = db.Column(db.Text)
    tags = db.relationship('Tag', secondary=article_tags, backref=db.backref('articles', lazy='dynamic'))
    is_published = db.Column(db.Boolean)
    is_deleted = db.Column(db.Boolean)
    is_headline = db.Column(db.Boolean)

    created_timestamp = db.Column(db.DateTime, server_default=timestamp(), nullable=False)
    modified_timestamp = db.Column(db.DateTime, server_default=timestamp(), onupdate=timestamp(), nullable=False)

    def json(self):
        return {
            'id': self.id,
            'title': self.title,
            'content': self.content,
            'author': self.author.name,
            'published': self.is_published,
            'deleted': self.is_deleted,
            'public_url': self.public_url,
            'created': {
                'date': self.format_date(self.created_timestamp),
                'time': self.format_time(self.created_timestamp)
            },
            'modified': {
                'date': self.format_date(self.modified_timestamp),
                'time': self.format_time(self.modified_timestamp)
            }
        }

    @staticmethod
    def format_date(timestamp):
        try:
            return timestamp.date().strftime("%d %b %Y")
        except:
            return "unknown"

    @staticmethod
    def format_time(timestamp):
        try:
            return timestamp.date().strftime("%H:%M:%S")
        except:
            return "unknown"


class Tag(db.Model):
    __tablename__ = "tags"

    name = db.Column(db.String(32), index=True, unique=True, primary_key=True, nullable=False)
