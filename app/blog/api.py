import datetime
import logging
import re
from uuid import UUID, uuid4
from flask import request, jsonify
from flask_login import login_required, current_user
from flask_misaka import Misaka
from app import db, utils
from app.api import api
from app.api.response import APIResponse
from app.blog.models import Article, Tag

log = logging.getLogger("blog.api")
log.setLevel(logging.DEBUG)


@api.route("/articles/all", methods=["POST"])
def articles_all():
    # log.debug("Invocation requested: articles_all()")
    articles = Article.query.all()
    response_data = []

    for article in articles:
        response_data.append(article.json())

    return APIResponse.success().data(response_data).json()


@api.route("/articles/get/<article_id>", methods=["POST"])
@login_required
def articles_get(article_id):
    log.debug("Invocation requested: articles_get(%s)" % article_id)

    article = Article.query.get(article_id)

    if article is not None:
        return APIResponse.success().data(article.json()).json()
    else:
        return APIResponse.failure().reason("No such article").json()


@api.route("/articles/publish/", methods=["POST"])
@login_required
def articles_publish():
    article_id = request.get_json()['id']
    log.debug("Invocation requested: articles_publish(%s)" % article_id)

    article = Article.query.get(article_id)

    if article is None:
        return APIResponse.failure().reason("No such article").json()

    article.is_published = True
    return persist_article(article)


@api.route("/articles/unpublish/", methods=["POST"])
@login_required
def articles_unpublish():
    article_id = request.get_json()['id']

    log.debug("Invocation requested: articles_unpublish(%s)" % article_id)

    article = Article.query.get(article_id)

    if article is None:
        return APIResponse.failure().reason("No such article").json()

    article.is_published = False
    return persist_article(article)


@api.route("/articles/save/", methods=["POST"])
@login_required
def articles_save():
    article_data = request.get_json()

    log.debug("Invocation requested: articles_save(%s)" % article_data)

    if not is_valid_uuid(article_data['id']):
        return APIResponse.failure().reason("Article ID is not valid.").json()

    article = Article.query.get(article_data['id'])

    if article is None:
        article = Article()
        article.id = article_data['id']
        article.author = current_user
        article.is_published = False
        article.is_deleted = False
        article.is_headline = False
        article.public_url = None
        article.modified_timestamp = datetime.datetime.utcnow()

    if 'content' in article_data.keys():
        article.content = article_data['content']
    else:
        article.content = ''

    if 'title' in article_data.keys():
        article.title = article_data['title']
    else:
        article.title = ''

    if article.public_url is None:
        public_url = article.title if article.title != '' else "New Article"
        article.public_url = unique_public_url(public_url, article)

    return persist_article(article)


@api.route("/articles/delete/", methods=["POST"])
@login_required
def articles_delete():
    article_id = request.get_json()['id']

    log.debug("Invocation requested: articles_delete(%s)" % article_id)

    article = Article.query.get(article_id)

    if article is not None:
        article.is_deleted = True
        return persist_article(article)
    else:
        return APIResponse.failure().reason("There is no article with that ID").json()


@api.route("/articles/markdown/", methods=["POST"])
@login_required
def articles_markdown():
    misaka = Misaka()
    log.debug("Invocation requested: articles_markdown(%s)" % request.data)
    response_data = {'markdown': misaka.render(request.data)}
    return APIResponse.success().data(response_data).json()


@api.route("article/tags/set/<article_id>/<tags>", methods=["POST"])
@login_required
def set_tags(article_id, tags):
    log.debug("Invocation requested: set_tags(%s, %s)" % (article_id, str(tags)))
    article = Article.query.get_or_404(article_id)

    tag_names = tags.split(",")
    tags = get_tags_from_names(tag_names)

    article.tags = tags
    db.session.add(article)
    db.session.commit()

    response = {"success": True, "tags": tag_names}

    return jsonify(response)


@api.route("article/tags/add/<article_id>/<tags>", methods=["POST"])
@login_required
def add_tags(article_id, tags):
    log.debug("Invocation requested: add_tags(%s, %s)" % (article_id, str(tags)))
    article = Article.query.get_or_404(article_id)

    tag_names = re.compile(",\s*").split(tags)
    tags = get_tags_from_names(tag_names)

    for tag in tags:
        if tag not in article.tags:
            article.tags.append(tag)

    db.session.add(article)
    db.session.commit()
    response = {"success": True, "tags": list(map(lambda x: x.name, article.tags))}

    return jsonify(response)


@api.route("article/tags/remove/<article_id>/<tag_name>", methods=["POST"])
@login_required
def remove_tag(article_id, tag_name):
    log.debug("Invocation requested: remove_tag(%s, %s)" % (article_id, tag_name))

    article = Article.query.get_or_404(article_id)
    tag_name = tag_name.strip()
    response = {}
    article_tag = None

    for tag in article.tags:
        if tag.name == tag_name:
            article_tag = tag
            break

    if article_tag is not None:
        try:
            article.tags.remove(article_tag)
            db.session.add(article)
            db.session.commit()
            response["success"] = True
            response["tags"] = list(map(lambda x: x.name, article.tags))
        except Exception as e:
            log.exception(e)
            db.session.rollback()
            response["success"] = False
            response["reason"] = str(e)
    else:
        response["success"] = False
        response["reason"] = "No such tag"

    return jsonify(response)


@api.route("api/tags/all/<article_id>/", methods=["POST"])
@login_required
def all_tags(article_id):
    log.debug("Invocation requested: all_tags(%s)" % article_id)
    article = Article.query.get(article_id)

    if article is not None:
        tags = list(map(lambda x: x.name, article.tags))
        return APIResponse.success().data({"tags": tags}).json()

    return APIResponse.failure().reason("No article with that ID").json()


@api.route("article/tags/get/<article_id>", methods=["POST"])
@login_required
def get_tags(article_id):
    log.debug("Invocation requested: get_tags(%s)" % article_id)
    article = Article.query.get_or_404(article_id)
    response = {"success": True, "tags": list(map(lambda x: x.name, article.tags))}

    return jsonify(response)


def persist_article(article, data=None):
    log.debug("Persisting article: %s" % article)

    # noinspection PyBroadException
    try:
        db.session.add(article)
        db.session.commit()
        response = APIResponse.success()
        if data is not None:
            response.data(data)
        return response.json()
    except Exception as e:
        log.exception(e)
        db.session.rollback()
        return APIResponse.failure().json()


def is_valid_uuid(uuid_string):
    try:
        val = UUID(uuid_string, version=4)
    except ValueError:
        return False

    return val.hex == uuid_string or str(val) == uuid_string


def unique_public_url(public_url, article=None):
    used_public_urls = Article.query.with_entities(Article.public_url).all()
    used_public_urls = list(map(lambda x: x[0], used_public_urls))
    public_url = utils.to_url(public_url)
    if article is not None and article.public_url in used_public_urls:
        used_public_urls.remove(article.public_url)
    public_url = utils.unique_name(public_url, lambda x: x not in used_public_urls)
    return public_url


def get_tags_from_names(tag_names):
    tags = []
    new_tags_created = False
    for tag_name in tag_names:
        tag_name = tag_name.strip()
        tag = Tag.query.filter(Tag.name == tag_name).first()
        if tag is None:
            tag = Tag()
            tag.id = str(uuid4())
            tag.name = tag_name
            db.session.add(tag)
        tags.append(tag)

    if new_tags_created:
        db.session.commit()

    return tags
