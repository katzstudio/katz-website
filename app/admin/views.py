from flask import redirect
from flask_login import login_required
from app.admin import admin


@admin.route("/")
@login_required
def admin():
    return redirect("/admin/articles/", code=302)
