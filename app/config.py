import os
import logging
from logging.handlers import RotatingFileHandler

LOG_FORMAT = '%(asctime)s %(levelname)-8s %(name)-12s %(message)s'

LOG = logging.getLogger('manage')
LOG.setLevel(logging.DEBUG)

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."));


def path_for(path):
    return os.path.join(basedir, path)


def get_configuration():
    configuration = 'bacau'
    if 'KATZ_CONFIG' in os.environ:
        configuration = os.environ['KATZ_CONFIG']
        LOG.info("Using app config '%s' from env." % configuration)
    else:
        LOG.info("Using default app config '%s'" % configuration)
    return configuration


def setup_logging():
    root_log = logging.getLogger()
    root_log.handlers = []
    root_log.addHandler(create_console_log_handler())
    root_log.addHandler(create_file_log_handler('logs/output.log'))

    werkzeug_log = logging.getLogger('werkzeug')
    werkzeug_log.setLevel(logging.INFO)
    werkzeug_log.handlers = []
    werkzeug_log.addHandler(create_file_log_handler('logs/access.log'))
    werkzeug_log.propagate = False


def create_console_log_handler():
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter(LOG_FORMAT)
    console_handler.setFormatter(formatter)
    return console_handler


def create_file_log_handler(output_file='output.log'):
    file_handler = RotatingFileHandler(os.path.join(basedir, output_file), maxBytes=100000000, backupCount=50)
    file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
    file_handler.setLevel(logging.DEBUG)
    return file_handler


class DefaultConfig:
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + path_for('db/katz.db')
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SECRET_KEY = os.environ.get('KATZ_SECRET_KEY') or '8acgn5pICyR07LEzawJVnptab7HoxpT3uyp5ADrpZIu6GiqNKyEblouPt5jXpzOU'

    GALLERY_UPLOAD_FOLDER = path_for('app/static/gallery')
    GALLERY_PREFERRED_RATIOS = [1, 0.5, 2, 3]

    TEMP_FOLDER = path_for("app/static/temp")

    ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

    DEBUG = True

    @staticmethod
    def init_app(app):
        # enable jinja line statement prefix
        app.jinja_env.line_statement_prefix = '%'

        app.logger.handlers = []


class LocalMysqlConfig(DefaultConfig):
    SQLALCHEMY_DATABASE_URI = "mysql://root:@localhost:3306/katzstudio"


class PythonAnywhereConfig(DefaultConfig):
    SQLALCHEMY_DATABASE_URI = os.environ.get("KATZ_DATABASE_URL")
    SQLALCHEMY_POOL_RECYCLE = 280
    SQLALCHEMY_POOL_TIMEOUT = 20


class BacauConfig(DefaultConfig):
    SQLALCHEMY_DATABASE_URI = "postgresql://alex:oscarwilde@94.177.136.21:5432/katzstudio"


config = {
    'default': LocalMysqlConfig,
    'localmysql': LocalMysqlConfig,
    'pythonanywhere': PythonAnywhereConfig,
    'bacau': BacauConfig
}
