from flask import render_template
from sqlalchemy import desc

from app.gallery.models import Image
from app.main import main
from app.blog.models import Article
from app.blog.views import get_headline_articles
from app.auth.models import User


@main.route("/")
def index():
    articles = Article.query.filter(Article.is_published == True).order_by(desc(Article.created_timestamp)).all()

    if len(articles) > 0:
        last_article = articles[-1]
    else:
        last_article = None

    if len(articles) > 1:
        second_last_article = articles[-2]
    else:
        second_last_article = None

    return render_template(
        "blog/index.html",
        articles=articles,
        article=last_article,
        previous_article=second_last_article,
        next_article=None,
        headlines=get_headline_articles())


@main.route("/team")
def team():
    return render_template(
        "team/index.html",
        headlines=get_headline_articles(),
        team=User.query.all())


@main.route("/gallery")
def gallery():
    images = Image.query.all()
    return render_template("gallery/show.html", images=images)


@main.route("/games")
def games():
    return "Not yet implemented"
