from flask import jsonify


class APIResponse:
    def __init__(self):
        self.__data = {}
        self.__status = {}

    @staticmethod
    def success():
        response = APIResponse()
        response.__status['success'] = True
        return response

    @staticmethod
    def failure():
        response = APIResponse()
        response.__status['success'] = False
        return response

    def reason(self, reason):
        self.__status['reason'] = reason
        return self

    def data(self, data):
        self.__data = data
        return self

    def json(self):
        return jsonify({'status': self.__status, 'data': self.__data})
