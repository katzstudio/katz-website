import logging
import os
import shutil
from uuid import uuid4

from sqlalchemy.exc import IntegrityError
from werkzeug.utils import secure_filename
from flask_login import login_required, current_user
from flask import request, url_for, current_app

from app import db
from app.api import api
from app.api.response import APIResponse
from app.gallery.img_util import img_copy, img_resize_half, img_get_size, img_get_preferred_ratio, img_save_first_frame
from app.gallery.views import image_path
from app.gallery.models import Image

log = logging.getLogger("gallery.api")
log.setLevel(logging.DEBUG)


@api.route("/gallery/all", methods=["GET", "POST"])
def gallery_all():
    images = Image.query.all()
    response_data = []

    for image in images:
        if image.total_size_on_disk is None:
            size = "0"
        else:
            size = image.total_size_on_disk

        response_data.append({
            'size': size,
            "id": image.id,
            "title": image.title,
            "description": image.description,
            "src": url_for('static', filename='gallery/' + image.get_filename_full()),
            "msrc": url_for('static', filename='gallery/' + image.get_filename_full()),
            "w": image.width,
            "h": image.height,
            "is_visible_in_gallery": image.is_visible_in_gallery,
            "banner_candidate": image.is_banner_candidate})

    return APIResponse.success().data(response_data).json()


@api.route("/gallery/image/delete/<image_id>", methods=["POST"])
@login_required
def gallery_image_delete(image_id):
    log.info("Requesting delete for image %s" % image_id)
    image = Image.query.get(image_id)

    if image is None:
        log.warn("Image with id: %s was not found." % image_id)
        return APIResponse.failure().reason("There's no image with that ID").json()

    # noinspection PyBroadException
    try:
        db.session.delete(image)
        db.session.commit()
    except:
        log.warn("Failed to remove %s from DB." % image)
        db.session.rollback()
        return APIResponse.failure().reason("Exception raised when removing image from the database").json()

    try:
        os.remove(image_path(image.get_filename_full()))
        os.remove(image_path(image.get_filename_half()))
        os.remove(image_path(image.get_filename_preview()))
    except FileNotFoundError:
        pass

    log.info("%s was deleted successfully." % image)
    return APIResponse.success().json()


@api.route("/gallery/image/get/<image_id>", methods=["GET", "POST"])
@login_required
def gallery_image_get(image_id):
    image = Image.query.get(image_id)

    if image is None:
        return APIResponse.failure().reason("There's no image with that ID").json()

    return APIResponse.success().data(image.json()).json()


@api.route("/gallery/image/update/<image_id>", methods=["POST"])
@login_required
def gallery_image_update(image_id):
    image = Image.query.get_or_404(image_id)
    image.from_json(request.get_json())

    response_data = {
        'errors': ["That name is already used by another image"]
    }

    try:
        db.session.add(image)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        return APIResponse.failure().reason("That name is already used by another image").data(response_data).json()

    return APIResponse.success().json()


@api.route("/gallery/upload", methods=["POST"])
@login_required
def gallery_upload():
    file = request.files['file']

    log.info("Upload requested for file: " + file.filename)

    # save temporary
    upload_filename = file.filename
    original_name, original_ext = os.path.splitext(secure_filename(upload_filename))
    original_ext = original_ext.lower()
    temp_name = original_name[:110] + original_ext
    path_temp = os.path.join(current_app.config["TEMP_FOLDER"], temp_name)
    file.save(path_temp)

    size_on_disk = 0

    # save real images
    if original_ext != ".gif":
        ext = ".jpeg"
        filename_full = original_name + ext
        path_full = image_path(filename_full)

        filename_half = original_name + "_half" + ext
        path_half = image_path(filename_half)

        img_copy(path_temp, path_full)
        img_resize_half(path_temp, path_half)
    else:
        ext = ".gif"
        filename_full = original_name + ext
        path_full = image_path(filename_full)
        shutil.copy(path_temp, path_full)

        filename_half = original_name + "_half" + ext
        path_half = image_path(filename_half)
        shutil.copy(path_temp, path_half)

        filename_preview = original_name + "_preview" + ext
        path_preview = image_path(filename_preview)
        img_save_first_frame(path_temp, path_preview)

        size_preview = os.stat(path_preview).st_size

        size_on_disk += size_preview

    size_full = os.stat(path_full).st_size
    size_half = os.stat(path_half).st_size
    size_on_disk += size_full + size_half

    # delete temporary
    os.remove(path_temp)

    # add model
    dimension = img_get_size(path_full)
    ratio = img_get_preferred_ratio(dimension, current_app.config["GALLERY_PREFERRED_RATIOS"])

    # noinspection PyBroadException
    try:
        image = Image(
                id=uuid4(),
                title=original_name,
                uploader=current_user.username,
                is_banner_candidate=False,
                filename=original_name,
                extension=ext[1:],
                total_size_on_disk=size_on_disk,
                width=dimension[0],
                height=dimension[1],
                preferred_ratio=ratio)

        db.session.add(image)
        db.session.commit()

        log.info("Image %s was saved to database" % image)
    except Exception as e:
        db.session.rollback()
        os.remove(path_full)
        os.remove(path_half)
        log.warn("Failed to save image to database. Original exception: %s" % e)
        return APIResponse.failure().reason("Failed to save image to DB").json()

    return APIResponse.success().json()
