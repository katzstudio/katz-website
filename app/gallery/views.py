import os
from flask import render_template, current_app
from flask_login import login_required
from app.gallery.models import Image
from app.admin import admin


def image_path(image_name):
    return os.path.join(current_app.config["GALLERY_UPLOAD_FOLDER"], image_name)


@admin.route("/gallery/")
@login_required
def gallery_admin():
    images = Image.query.all()
    page = {
        'js': ["gallery/views/image_list.js"],
        'title': 'Gallery',
        'subtitle': 'Some pretty pictures'
    }
    return render_template("admin.html", page=page, images=images)
