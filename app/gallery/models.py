from app import db


class Image(db.Model):
    __tablename__ = "images"

    id = db.Column(db.String(36), primary_key=True)

    title = db.Column(db.String(128), unique=True, index=True)
    description = db.Column(db.Text)

    uploader = db.Column(db.String(32), db.ForeignKey("users.username"))

    is_banner_candidate = db.Column(db.Boolean)
    is_visible_in_gallery = db.Column(db.Boolean)

    filename = db.Column(db.String(128), nullable=False)
    extension = db.Column(db.String(8), nullable=False)

    total_size_on_disk = db.Column(db.Integer)

    preferred_ratio = db.Column(db.Numeric(2), nullable=False)
    width = db.Column(db.Integer, nullable=False)
    height = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return "<Image " + ", ".join(["%s:%r" % (c.name, getattr(self, c.name)) for c in self.__table__.columns]) + ">"

    def json(self):
        return {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'bannerCandidate': self.is_banner_candidate,
            'galleryItem': self.is_visible_in_gallery,
            'imageType': 'Artwork'
        }

    def from_json(self, json):
        self.title = json['title']
        self.description = json['description']
        self.is_banner_candidate = json['bannerCandidate']
        self.is_visible_in_gallery = json['galleryItem']

    def get_filename_full(self):
        return self.filename + "." + self.extension

    def get_filename_half(self):
        return self.filename + "_half" + "." + self.extension

    def get_filename_preview(self):
        return self.filename + "_preview" + "." + self.extension
