from flask import url_for
from .models import Image
from random import random


class GalleryContext:
    DEFAULT_BANNER_URL = "img/default_banner.png"

    @staticmethod
    def random_banner_image():
        query = Image.query.filter(Image.is_banner_candidate == True)
        banner_count = int(query.count())
        if banner_count > 0:
            random_image = query.offset(int(banner_count * random())).first()
            return url_for('static', filename='gallery/' + random_image.get_filename_full())
        else:
            return url_for('static', filename=GalleryContext.DEFAULT_BANNER_URL)
