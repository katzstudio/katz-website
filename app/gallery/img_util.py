from PIL import Image


def img_copy(input_path, output_path):
    Image.open(input_path).save(output_path, quality=93, optimize=True)


def img_resize_half(input_path, output_path):
    image = Image.open(input_path)
    size = image.size
    image.resize((int(size[0] / 2), int(size[1] / 2)), Image.BICUBIC).save(output_path, quality=93, optimize=True)


def img_save_first_frame(gif_path, first_frame_path):
    im = Image.open(gif_path)
    im.convert("RGBA")
    im.save(first_frame_path, quality=93, optimize=True)


def img_get_size(image_path):
    return Image.open(image_path).size


def img_get_preferred_ratio(size, ratios):
    real_ratio = float(size[0]) / size[1]
    best_candidate = ratios[0]
    for ratio_candidate in ratios:
        if abs(real_ratio - ratio_candidate) < abs(real_ratio - best_candidate):
            best_candidate = ratio_candidate
    return best_candidate
