#!/usr/bin/env python
from flask_script import Manager, Shell, Server
from app import create_app, db
from app.blog.models import Article
from app.config import setup_logging, get_configuration
from app.init_db import InitializeDatabase


def shell_context():
    return dict(app=app, db=db, Article=Article)


setup_logging()

app = create_app(get_configuration())

manager = Manager(app)
manager.add_command("shell", Shell(make_context=shell_context))
manager.add_command("runserver", Server(host="0.0.0.0", port=8080))
manager.add_command("init_db", InitializeDatabase)

if __name__ == "__main__":
    manager.run()
